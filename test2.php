<?php
if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}
session_start();
include("../../controllers/setup/connect.php");
/*
if($_SESSION['access_level']!='admin')
{
    exit("unauthorized");
}
*/
?>
<nav aria-label="breadcrumb">
     <ol class="breadcrumb">
       <li class="breadcrumb-item active" aria-current="page">End Product Mangement</li>
     </ol>
</nav>


<div class="col-lg-12 col-xs-12">
  <div class="card card-primary card-outline">
    <div class="card-header">
      End Product
      <button class="btn btn-link" style="float:right;"
              data-toggle="modal" data-target="#add-end-product-modal">
              <i class="fa fa-plus-circle"></i> Add End Product For a Project
      </button>
    </div>
    <div class="card-body table-responsive">

      <?php
   $sql_query1 =  mysqli_query($dbc,"SELECT * FROM end_product ORDER BY id ");

   $no = 1;
   if($total_rows1 = mysqli_num_rows($sql_query1) > 0)
   {?>

     <table class="table table-striped table-bordered table-hover" id="end-product-table" style="width:100%">
       <thead>
         <tr>
           <td>#</td>
           <td>Product Name</td>
           <td>Unit Price</td>
           <td>Total Stock</td>
           <td>Available Stock</td>
           <td>Total</td>
           <td>Start Date</td>
           <td>End Date</td>
           <td>Duration</td>
            <td>Project Name</td>

           <td>Sample Image</td>
         </tr>
       </thead>
       <?php
          $no = 1;
          $sql= mysqli_query($dbc,"SELECT * FROM end_product ORDER BY id DESC");
          while($product = mysqli_fetch_array($sql))
          {
            ?>
            <tr style="cursor: pointer;">
              <td width="40px"><?php echo $no++ ;?>.

              </td>

              <td>

                <a class="" href="#" data-toggle="modal" data-target="#stock-util-modal-<?php echo $product['id'];?>"
                  title="Click on <?php echo $product['product_name'];?> to view Stocks Used For prodcution">
                <span class="text-primary" style="cursor:pointer;"><?php echo $product['product_name'];?></span>
                </a>

                <!-- invoice payment Modal -->


                <div class="modal fade" id="stock-util-modal-<?php echo $product['id'];?>" role="dialog">
                  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Stock Item Used To Manufacture <strong><?php echo $product['product_name'];?></strong>
                          <?php
                       $sql_query2 =  mysqli_query($dbc,"SELECT * FROM single_product WHERE product_name ='".$product['id']."'");

                        // $no = 1;
                       if($total_rows2 = mysqli_num_rows($sql_query2) > 0)
                       {?>

                          <div class="row">
                              <div class="col-lg-4 col-xs-12 form-group">
                                <label> Stock Cost:</label>

                              <?php

                                $debit_amt2 = mysqli_query($dbc,"SELECT * FROM single_product WHERE product_name ='".$product['id']."' ORDER BY id");
                                if(mysqli_num_rows($debit_amt2) > 0)
                                {

                                     $result2 = mysqli_query($dbc, "SELECT sum(total) as tot FROM single_product WHERE product_name ='".$product['id']."' ORDER BY id"  );


                                       while($stock_cost = mysqli_fetch_assoc($result2))
                                       {
                                          ?>

                                         <input type="text"  class="select2 form-control new_stock_remaining"  value ="<?php echo $stock_cost['tot'];?>" name="qtt" readonly>
                                         <?php
                                    //    echo $stock_cost['tot'];
                                      }
                                }
                                     ?>

                                </div>
                                <div class="col-lg-4 col-xs-12 form-group">
                                  <label>Product Cost</label>

                                  <?php

                                  $debit_amt = mysqli_query($dbc,"SELECT * FROM end_product WHERE id ='".$product['id']."' ORDER BY id");
                                  if(mysqli_num_rows($debit_amt) > 0)
                                  {

                                       $result = mysqli_query($dbc, "SELECT sum(total) as tot FROM end_product WHERE id ='".$product['id']."' ORDER BY id"  );

                                         while($debit = mysqli_fetch_assoc($result))
                                         {
                                             ?>
                                           <input type="text"  class="select2 form-control stock_qtt"  value ="<?php echo $debit['tot'];?>" name="qtt" readonly>
                                           <?php
                                         }
                                  }
                                       ?>

                                    </div>
                                  <div class="col-lg-4 col-xs-12 form-group">
                                    <label>Total
                                      <?php
                                $result2 = mysqli_query($dbc, "SELECT sum(total) as tot FROM single_product WHERE product_name ='".$product['id']."' ORDER BY id"  );

                                  while($stock_cost = mysqli_fetch_assoc($result2))
                                  {
                                // echo $stock_cost['tot'];

                                 $result = mysqli_fetch_assoc(mysqli_query($dbc, "SELECT sum(total) as tot2 FROM end_product WHERE id ='".$product['id']."' ORDER BY id" ) );

                                   $profit = $result['tot2'] - $stock_cost['tot'];
                                   if($profit > 0)
                                   {
                                     ?>
                                    <strong> Profit</strong>
                                   <?php
                                 }
                                 else
                                 {
                                   ?>
                                  <strong> Loss</strong>
                                 <?php
                               }

                                 }
                                 ?>
                                      </label>
                                          <?php
                                    $result2 = mysqli_query($dbc, "SELECT sum(total) as tot FROM single_product WHERE product_name ='".$product['id']."' ORDER BY id"  );

                                      while($stock_cost = mysqli_fetch_assoc($result2))
                                      {
                                    // echo $stock_cost['tot'];

                                     $result = mysqli_fetch_assoc(mysqli_query($dbc, "SELECT sum(total) as tot2 FROM end_product WHERE id ='".$product['id']."' ORDER BY id" ) );

                                       ?>

                                        <input type="text"  class="select2 form-control stock_qtt"  value ="<?php echo $profit;?>" name="qtt" readonly>
                                           <?php
                                      //echo $profit;

                                   //    echo $stock_cost['tot'];
                                     }
                                     ?>
                                          </div>

                          </div>

                          <?php
                          }
                          else
                          {
                                ?>

                            <?php
                          }
                          ?>
                             <span class="font-weight-bold"></h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <?php
                     $sql_query =  mysqli_query($dbc,"SELECT * FROM single_product WHERE product_name ='".$product['id']."'");

                      // $no = 1;
                     if($total_rows = mysqli_num_rows($sql_query) > 0)
                     {?>

                        <table class="table table-striped table-bordered table-hover stock-util-table" style="width:100%">

                            <thead>
                            <tr>
                              <th>#</th>
                              <th>Stock Name</th>
                              <th>Quantity Used</th>
                              <th>Unit Price</th>
                              <th>Total</th>
                              <th>Date requested</th>
                              <th>Recorded By</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                             $no = 1;
                            $stock_row = mysqli_query($dbc,"SELECT * FROM single_product WHERE product_name ='".$product['id']."' ORDER BY id");

                            while($stock_used = mysqli_fetch_array($stock_row))
                            {
                              ?>
                             <tr style="cursor: pointer;">
                                <td width="50px"> <?php echo $no++;?>.

                                </td>
                                <td>
                                  <?php

                                       $result = mysqli_query($dbc, "SELECT * FROM stock_item WHERE reference_no ='".$stock_used['end_product_ref']."' ORDER BY id "  );
                                       if(mysqli_num_rows($result))
                                       {
                                         while($stockist = mysqli_fetch_array($result))
                                         {

                                            echo $stockist['item_name'];

                                         }
                                       }
                                       ?>

                                     </td>
                                     <td><?php echo $stock_used['qtt'];?></td>
                                     <td><?php echo $stock_used['unit_price'];?></td>
                                      <td><?php echo $stock_used['total'];?></td>
                                      <td><?php echo $stock_used['time_recorded'];?></td>
                                      <td><?php echo $stock_used['recorded_by'];?></td>


                              </tr>
                              <?php
                            }
                             ?>
                          </tbody>


                                        <tfoot style="background:silver;">
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>

                                                </tr>
                                            </tfoot>
                          </table>
                          <?php
                          }
                          else
                          {
                                ?>
                              <br/>
                <div class="alert alert-info">
                  <strong><i class="fa fa-info-circle"></i> No stocks attached to the end Product</strong>
                </div>

                            <?php
                          }
                          ?>

                        </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                      </div>
                    </div>
                    </div>

                <!-- end invoice payment modal  -->


                </td>
              <td><?php echo $product['unit_price'];?></td>
              <td><?php echo $product['qtt'];?></td>
              <td><?php echo $product['qtt'];?></td>
              <td><?php echo $product['total'];?></td>
              <td><?php echo $product['start_date'];?></td>
              <td><?php echo $product['end_date'];?></td>
              <td><?php echo $product['duration'];?></td>
              <td>
                <?php

                     $result = mysqli_query($dbc, "SELECT * FROM customer WHERE id ='".$product['customer_id']."' ORDER BY id "  );
                     if(mysqli_num_rows($result))
                     {
                       while($project= mysqli_fetch_array($result))
                       {

                          echo $project['customer_name'];

                       }
                     }
                     ?>


                   </td>

                   <td>          <?php
                             $profile_pic = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Email ='".$_SESSION['email']."'"));
                             ?>
                               <img width="30%" height="10%"src="assets/img/<?php echo $profile_pic['emp_photo']; ?>"  alt="User Image">

                              </td>

            </tr>
            <?php
          }
        ?>
     </table>

     <?php
     }
     else
     {
           ?>
         <br/>
<div class="alert alert-info">
<strong><i class="fa fa-info-circle"></i> No End Product Recorded</strong>
</div>

       <?php
     }
     ?>



    </div>
  </div>
</div>


<!-- start add end product modal -->
<div class="modal fade" id="add-end-product-modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">


        <h5 class="modal-title">Adding End Product for a Project
           <span class="font-weight-bold"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-end-product-form">

          <input type="hidden" name="add-end-product" value="add-end-product">
          <!-- start of row -->
          <div class="row">


              <div class="col-lg-3 col-xs-12 form-group">
                  <label><span class="required">*</span>Product Name</label>
                  <input type="text" placeholder="Product Name" name="product_name">
              </div>
              <div class="col-lg-3 col-xs-12 form-group">
                  <label><span class="required">*</span>Unit Price</label>

                    <input type="number" autocomplete="off" class="select2 form-control stock_unit_price"   name="unit_price">
              </div>
              <div class="col-lg-3 col-xs-12 form-group">
                  <label><span class="required">*</span>Qtt</label>

                    <input type="number" autocomplete="off" class="select2 form-control stock_qtt"   name="qtt">
              </div>
              <div class="col-lg-3 col-xs-12 form-group">
                  <label><span class="required">*</span>Total</label>

                    <input type="number" autocomplete="off" class="select2 form-control stock_total"  name="total" readonly>
              </div>
              </div>
              <div class="row">
              <div class="col-lg-3 col-xs-12 form-group">
                <label> <span class="required">*</span> Start Date</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fal fa-calendar-day"></i></div>
                  </div>
                  <input type="text" class="form-control project_start_date" autocomplete="off" name="product_start_date" required>
                </div>
              </div>
              <div class="col-lg-3 col-xs-12 form-group">
                <label> <span class="required">*</span> End Date</label>
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fal fa-calendar-day"></i></div>
                  </div>
                  <input type="text" class="form-control project_end_date" autocomplete="off" name="product_end_date" required>
                </div>
              </div>
              <div class="col-lg-3 col-xs-12 form-group">
                  <label><span class="required">*</span>Duration</label>


                  <input type="hidden" class="form-control project-duration-in-days" name="duration" readonly required>
                  <input type="text" class="form-control pull-right project-duration bg-grey" readonly required>
              </div>

              <div class="col-lg-3 col-xs-12 form-group">
                <label for="project"><span class="required">*</span>Project Name</label>
                <?php
                $result = mysqli_query($dbc, "SELECT * FROM customer");
                echo '
                <select name="customer_id" class="select2 form-control" data-placeholder="Select project Name" required>
                <option></option>';
                while($row = mysqli_fetch_array($result)) {
                  // we're sending the strategic objective id to the db
                    echo '<option value="'.$row['id'].'">'.$row['customer_name']."</option>";
                }
                echo '</select>';
                ?>
              </div>

          </div>
          <!-- end of row -->

          <div class="row text-center">
              <button type="submit" class="btn btn-primary btn-block">SUBMIT</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end add project lesson learnt modal -->
