<?php
  session_start();
  include("../../controllers/setup/connect.php");

  if(!$_SERVER['REQUEST_METHOD'] == "POST")
  {
    exit();
  }

 ?>

 <table class="table table-bordered table-striped table-hover" id="dashboard-active-stocks-table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Stock Name</th>
      <th scope="col">Quantity Available</th>
      <th scope="col">Supplier Name</th>
      <th scope="col">Status</th>
      <th scope="col">Recorded By</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
     $sql = mysqli_query($dbc,"SELECT * FROM stock_item  ORDER BY id DESC");
     while($row = mysqli_fetch_array($sql)){
     ?>
          <tr style="cursor: pointer;">
            <th scope="row"><?php echo $no++;?></th>
            <td> <small><?php echo $row['item_name'] ;?></small></td>
            <td><small>


                   <?php

                   $Proj_phase = mysqli_query($dbc,"SELECT * FROM single_product WHERE end_product_ref ='".$row['reference_no']."' ORDER BY id");
                   if(mysqli_num_rows($Proj_phase) > 0)
                   {

                     $single_product = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM single_product WHERE end_product_ref ='".$row['reference_no']."' ORDER BY id DESC LIMIT 1"));

                 echo $single_product['stock_remaining'];
                   ?>

                 <?php

                     }

                      else {

                             $result = mysqli_query($dbc, "SELECT * FROM invoice_received WHERE reference_no ='".$row['reference_no']."' ORDER BY id "  );
                             if(mysqli_num_rows($result))
                             {
                               while($product_qtt = mysqli_fetch_array($result))
                               {

                                  echo $product_qtt['qtt'];

                               }
                             }
                             ?>
                        <?php

                      }
                      ?>
                    </small>
                 </td>
            <td><small>
              <?php
                        echo $row['supplier_id'];

                   ?>

                 </small>
                 </td>
                 <td> <small><?php echo $row['status'] ;?></small></td>
                    <td> <small><?php echo $row['recorded_by'] ;?></small></td>

          </tr>
          <?php
        }
     ?>
  </tbody>
</table>
<script>
$("[data-toggle=popover]").popover();
</script>
