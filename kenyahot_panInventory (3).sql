-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2022 at 12:28 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kenyahot_panInventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(6) NOT NULL,
  `email` varchar(50) NOT NULL,
  `action_name` varchar(100) NOT NULL,
  `action_reference` varchar(3000) NOT NULL,
  `action_icon` varchar(100) DEFAULT NULL,
  `page_id` varchar(100) NOT NULL,
  `time_recorded` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `email`, `action_name`, `action_reference`, `action_icon`, `page_id`, `time_recorded`) VALUES
(181, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 13:27:41'),
(182, 'inventory@panoramaengineering.com', 'Supplier Creation', 'Added a supplier ZHEJIANG YOMIN ELECTRIC LTD', 'fad fa-pennant text-info', 'supplier-management-link', '2020/07/15 13:45:26'),
(183, 'inventory@panoramaengineering.com', 'Supplier Creation', 'Added a supplier CHUANG XIONG GROUP TECHNOLOGY LTD', 'fad fa-pennant text-info', 'supplier-management-link', '2020/07/15 13:48:31'),
(184, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Copper lug compress', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 13:52:07'),
(185, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Copper lug  compress', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:00:37'),
(186, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Copper lug compress', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:02:38'),
(187, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Expulsion Fuse-cut-out', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:06:20'),
(188, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Expulsion Fuse-cut-out', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:07:19'),
(189, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 14:24:12'),
(190, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Termination Kits', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:25:47'),
(191, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Termination Kits', 'far fa-project-diagram text-success', 'stock-management-link', '2020/07/15 14:26:39'),
(192, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 15:22:17'),
(193, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 15:25:23'),
(194, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 15:38:57'),
(195, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/15 15:40:07'),
(196, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/16 10:21:53'),
(197, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/16 20:37:01'),
(198, 'osino@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/17 15:42:37'),
(199, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/17 19:23:02'),
(200, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/17 19:56:37'),
(201, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/19 17:58:38'),
(202, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/20 13:31:05'),
(203, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/22 12:15:47'),
(204, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/22 13:32:12'),
(208, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/22 13:49:54'),
(209, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/07/23 14:22:09'),
(210, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/03 14:52:56'),
(211, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/04 09:05:53'),
(212, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/04 15:26:26'),
(213, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals7', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/04 15:55:12'),
(214, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/06 16:10:17'),
(215, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals6', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/06 16:19:02'),
(216, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals3', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/06 16:20:49'),
(217, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/10 11:22:07'),
(218, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals5', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/10 11:29:05'),
(219, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals1', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/10 11:30:35'),
(220, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals2', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/10 11:31:13'),
(221, 'danson@panoramaengineering.com', 'Stock Approvals', 'approved Stock Item Approvals4', 'far fa-check-circle text-success', 'risk-approvals-link', '2020/08/10 11:31:50'),
(222, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/12 12:38:12'),
(223, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/12 15:10:55'),
(224, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/12 15:12:32'),
(225, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/13 12:11:06'),
(226, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/13 12:21:37'),
(227, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/14 09:43:40'),
(228, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/14 10:00:10'),
(229, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/14 14:09:08'),
(230, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/14 15:29:44'),
(231, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Central Auto & Hardware Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/14 15:31:18'),
(232, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item GI Sheet ', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/14 15:32:52'),
(233, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Kenchic Ltd', 'fad fa-pennant text-info', 'customer-management-link', '2020/08/14 15:35:17'),
(234, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductGI cover [cone] for duct', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/08/14 15:36:53'),
(235, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock GI Sheet  was Requested For the End Product 7', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/08/14 15:37:49'),
(236, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/15 09:06:24'),
(237, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/15 11:06:42'),
(238, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/15 12:16:50'),
(239, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/17 09:32:57'),
(240, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/18 11:38:30'),
(241, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/18 14:02:25'),
(242, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/18 14:29:08'),
(243, '', 'Logged out', 'Logged out of the system', 'far fa-sign-out text-danger', 'log-out-link', '2020/08/18 16:33:11'),
(244, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/18 16:33:19'),
(245, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/20 10:42:22'),
(246, '', 'Logged out', 'Logged out of the system', 'far fa-sign-out text-danger', 'log-out-link', '2020/08/20 12:49:45'),
(247, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/20 13:42:32'),
(248, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/21 09:36:45'),
(249, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/24 15:32:58'),
(250, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/24 15:43:57'),
(251, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/25 08:58:43'),
(252, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/25 12:21:47'),
(253, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Nail N Steels Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/25 12:23:49'),
(254, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Tee', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:27:36'),
(255, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item flat Bar ', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:28:46'),
(256, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item GI Sheet', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:30:37'),
(257, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Kens Metal Industries Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/25 12:32:51'),
(258, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MIld steel Dia', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:35:50'),
(259, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Stainless steel Rod', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:39:10'),
(260, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier P&L Above Heights Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/25 12:41:00'),
(261, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Werods', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:42:52'),
(262, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Zed Angle', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:44:20'),
(263, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:45:26'),
(264, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:46:24'),
(265, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Hinges', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:47:29'),
(266, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flat Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/25 12:48:53'),
(267, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/25 13:12:41'),
(268, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/26 09:59:37'),
(269, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/26 14:41:45'),
(270, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/28 11:25:36'),
(271, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/28 11:31:07'),
(272, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:49:12'),
(273, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:50:07'),
(274, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:50:47'),
(275, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:51:32'),
(276, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Noble Gases', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/28 11:52:52'),
(277, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item GAS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:54:23'),
(278, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Kansai Paint', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/28 11:57:27'),
(279, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Powder Coating Paint', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 11:59:04'),
(280, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/08/28 13:40:44'),
(281, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Maisha Steel  Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/28 14:03:49'),
(282, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flat Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 14:04:37'),
(283, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Clerb Enterprises Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/08/28 14:06:47'),
(284, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Expanded Mesh', 'far fa-project-diagram text-success', 'stock-management-link', '2020/08/28 14:08:58'),
(285, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/02 09:41:53'),
(286, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/02 14:42:50'),
(287, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/03 15:20:26'),
(288, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/04 15:41:34'),
(289, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/08 15:09:10'),
(290, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/08 17:56:07'),
(291, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/11 10:02:53'),
(292, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/16 15:34:46'),
(293, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/17 09:20:11'),
(294, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:22:09'),
(295, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flat Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:23:46'),
(296, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flat Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:24:33'),
(297, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:26:05'),
(298, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item RHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:34:00'),
(299, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Round Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:35:00'),
(300, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Z Purlin', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:35:51'),
(301, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Tee', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 09:37:20'),
(302, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Customer-Window', 'fad fa-pennant text-info', 'customer-management-link', '2020/09/17 09:55:42'),
(303, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Customer _ Roofing', 'fad fa-pennant text-info', 'customer-management-link', '2020/09/17 09:56:16'),
(304, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductWindow', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 09:58:21'),
(305, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Tee was Requested For the End Product 8', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:00:14'),
(306, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Flat Bar was Requested For the End Product 8', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:01:07'),
(307, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Angleline', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:03:29'),
(308, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Orbital Fastner', 'fad fa-pennant text-info', 'supplier-management-link', '2020/09/17 10:06:19'),
(309, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier P carlos hardware', 'fad fa-pennant text-info', 'supplier-management-link', '2020/09/17 10:07:12'),
(310, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Bolt & Nuts', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:08:13'),
(311, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Screws', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:09:20'),
(312, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Rawl Bolt', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:10:20'),
(313, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Down Stoppers', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:11:07'),
(314, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Hidges', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:11:45'),
(315, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductRoofing', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:14:19'),
(316, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Screws was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:15:32'),
(317, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Bolt & Nuts was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:16:13'),
(318, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Rawl Bolt was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:16:46'),
(319, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Z Purlin was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:17:16'),
(320, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Round Bar was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:17:49'),
(321, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Angleline was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:19:23'),
(322, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock RHS was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:19:59'),
(323, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock SHS was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:20:51'),
(324, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier GYM  Car Auto Paints', 'fad fa-pennant text-info', 'supplier-management-link', '2020/09/17 10:22:28'),
(325, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Paint', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:23:13'),
(326, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Thinner', 'far fa-project-diagram text-success', 'stock-management-link', '2020/09/17 10:23:53'),
(327, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Paint was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:24:39'),
(328, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Thinner was Requested For the End Product 9', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/17 10:25:13'),
(329, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/17 10:42:24'),
(330, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/18 11:07:58'),
(333, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/09/18 14:43:59'),
(334, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock Flat Bar was Requested For the End Product 8', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/09/18 15:20:27'),
(335, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/01 16:22:44'),
(336, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/01 16:29:05'),
(337, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/07 09:30:45'),
(338, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/07 10:29:53'),
(339, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Mild steel', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:37:59'),
(340, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Gas', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:38:55'),
(341, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:40:06'),
(342, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Akzonobel', 'fad fa-pennant text-info', 'supplier-management-link', '2020/10/07 10:43:19'),
(343, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Powder coating Paint', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:44:11'),
(344, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Rod', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:46:11'),
(345, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Mild Steel', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:47:41'),
(346, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Monross Hardware', 'fad fa-pennant text-info', 'supplier-management-link', '2020/10/07 10:51:10'),
(347, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SHS', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:52:05'),
(348, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Agnleline ', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:52:54'),
(349, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flat bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:53:33'),
(350, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Zed bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/07 10:54:15'),
(351, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/09 09:25:16'),
(352, '', 'Logged out', 'Logged out of the system', 'far fa-sign-out text-danger', 'log-out-link', '2020/10/12 09:14:49'),
(353, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/21 14:46:33'),
(354, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier APEX STEEL LTD', 'fad fa-pennant text-info', 'supplier-management-link', '2020/10/21 14:49:00'),
(355, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/21 14:54:30'),
(356, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/21 14:55:34'),
(357, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Mild Steel Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/21 14:56:20'),
(358, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Mild steel Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/21 14:57:13'),
(359, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Mild Steel Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/21 14:57:51'),
(360, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/27 12:19:46'),
(361, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/27 12:28:51'),
(362, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/27 12:29:46'),
(363, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Customer_ Trucking', 'fad fa-pennant text-info', 'customer-management-link', '2020/10/27 12:32:27'),
(364, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Bata Shoe Company', 'fad fa-pennant text-info', 'customer-management-link', '2020/10/27 12:34:13'),
(365, 'mbugua@panoramaengineering.com', 'Client Creation', 'Added a client Afrimac Engineering Ltd', 'fad fa-pennant text-info', 'customer-management-link', '2020/10/27 12:35:26'),
(366, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductTrucking 150mm', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/10/27 12:36:18'),
(367, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductShanks Size  115', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/10/27 12:38:26'),
(368, 'mbugua@panoramaengineering.com', 'End Product Creation', 'Added an End ProductSS Storange Tank', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/10/27 12:43:18'),
(369, 'mbugua@panoramaengineering.com', 'Stock Item Request', 'Stock SS plate was Requested For the End Product 12', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/10/27 12:44:55'),
(370, 'danson@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/27 14:30:22'),
(371, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/28 11:27:57'),
(372, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Shaft', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/28 11:39:02'),
(373, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Sheet', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/28 11:40:21'),
(374, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Round tube', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/28 11:41:39'),
(375, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Bend', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/28 11:42:38'),
(376, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Nasib Industries Ltd', 'fad fa-pennant text-info', 'supplier-management-link', '2020/10/28 11:44:58'),
(377, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Powder Coating Paint', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/28 11:47:14'),
(378, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/10/29 09:40:31'),
(379, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Fujjo Hardware', 'fad fa-pennant text-info', 'supplier-management-link', '2020/10/29 09:42:14'),
(380, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Filler Wire', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:43:12'),
(381, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Filler wire', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:43:56'),
(382, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Filler wire', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:44:08'),
(383, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Filler wire', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:44:12'),
(384, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Filler wire', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:44:12'),
(385, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Slit Cutter', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:45:23'),
(386, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Pure Copper Rod', 'far fa-project-diagram text-success', 'stock-management-link', '2020/10/29 09:47:08'),
(387, 'mbugua@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/11/04 12:04:03'),
(388, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Pipe G304', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:05:17'),
(389, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item MS Plate', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:06:56'),
(390, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Gas', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:08:48'),
(391, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Bend', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:11:01'),
(392, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Round Tube', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:12:08'),
(393, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Rosette cover', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:13:13'),
(394, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item SS Glass clamp', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:14:01'),
(395, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Wax Bar', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:16:40'),
(396, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flap Disc', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:17:29'),
(397, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Slit cutter', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:18:24'),
(398, 'mbugua@panoramaengineering.com', 'Supplier Creation', 'Added a supplier Jumbo Hardware', 'fad fa-pennant text-info', 'supplier-management-link', '2020/11/04 12:19:31'),
(399, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Flap wheel', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:24:35'),
(400, 'mbugua@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Sisal Cloth Disc', 'far fa-project-diagram text-success', 'stock-management-link', '2020/11/04 12:43:53'),
(401, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/11/06 11:53:45'),
(402, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/11/15 17:22:40'),
(403, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/11/24 11:23:26'),
(404, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/04 11:31:17'),
(405, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/04 13:24:34'),
(406, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/04 14:28:36'),
(407, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/05 15:10:40'),
(408, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/07 08:04:41'),
(409, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/07 10:21:45'),
(410, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/07 17:32:54'),
(411, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Test', 'far fa-project-diagram text-success', 'stock-management-link', '2020/12/07 17:35:32'),
(412, 'inventory@panoramaengineering.com', 'End Product Creation', 'Added an End ProductTest end pro', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/12/07 17:37:20'),
(413, 'inventory@panoramaengineering.com', 'Stock Item Request', 'Stock Test was Requested For the End Product 13', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/12/07 17:38:15'),
(414, 'inventory@panoramaengineering.com', 'End Product Creation', 'Added an End ProductPor', 'fad fa-pennant text-info', 'project-issue-logs-tab', '2020/12/07 17:39:21'),
(415, 'inventory@panoramaengineering.com', 'End product Delivery', 'Delivered Product with namePor', 'fad fa-pennant text-info', 'product-delivery-ta', '2020/12/07 17:39:39'),
(416, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/09 10:51:08'),
(417, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/11 16:53:47'),
(418, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/12 00:54:07'),
(419, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/18 09:29:36'),
(420, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2020/12/22 10:48:07'),
(421, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/02/08 10:42:10'),
(422, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/02/12 15:26:42'),
(423, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/02/25 20:48:23'),
(424, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/03/31 20:37:16'),
(425, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/04/06 14:55:39'),
(426, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/04/14 13:16:57'),
(427, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/07/02 08:58:17'),
(428, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/07/15 12:12:27'),
(429, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/07/22 09:00:47'),
(430, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/07/25 19:51:52'),
(431, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/07/26 12:55:09'),
(432, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/08/16 13:35:20'),
(433, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/08/17 08:20:45'),
(434, 'inventory@panoramaengineering.com', 'Invoice Payment', 'Paid an invoice with transaction id of  tryinv', 'fal fa-users-medical text-success', 'project-payments-plan-tab', '2021/08/17 08:23:58'),
(435, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/08/24 07:51:44'),
(436, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/09/05 00:26:48'),
(437, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2021/09/06 10:57:37'),
(438, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/01/25 09:57:05'),
(439, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 08:24:08'),
(440, 'inventory@panoramaengineering.com', 'New User', 'Added a new userJUSTUS M PAN006', 'far fa-user-plus', 'admin-user-management-link text-success', '2022/02/14 08:46:04'),
(441, 'justus@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 08:47:35'),
(442, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 08:57:15'),
(443, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 08:57:50'),
(444, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 12:22:56'),
(445, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 12:33:37'),
(446, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 15:40:11'),
(447, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/14 15:41:57'),
(448, 'inventory@panoramaengineering.com', 'Stock Creation', 'Added a Stock Item Test Stock', 'far fa-project-diagram text-success', 'stock-management-link', '2022/02/14 15:43:38'),
(449, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 13:41:51'),
(450, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 13:44:18'),
(451, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:09:40'),
(452, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:13:00'),
(453, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:17:44'),
(454, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:18:11'),
(455, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:23:15'),
(456, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:43:23'),
(457, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 14:58:09'),
(458, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:17:21'),
(459, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:18:58'),
(460, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:19:35'),
(461, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:20:22'),
(462, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:21:26'),
(463, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:32:26'),
(464, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:33:24'),
(465, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/15 15:33:54'),
(466, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:41:14'),
(467, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/15 15:41:50'),
(468, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 15:58:07'),
(469, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/15 16:10:55'),
(470, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/15 16:13:53'),
(471, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/15 16:20:24'),
(472, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/15 22:39:32'),
(473, 'justus@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/18 10:03:05'),
(474, 'justus@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/18 10:03:52'),
(475, 'justus@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/18 10:05:11'),
(476, 'justus@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/18 10:07:17'),
(477, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/19 21:02:30'),
(478, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/20 14:59:36'),
(479, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/20 19:34:31'),
(480, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/20 19:40:58'),
(481, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/20 20:05:31'),
(482, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/20 20:08:54'),
(483, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/21 09:21:41'),
(484, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/21 17:42:40'),
(485, 'inventory@panoramaengineering.com', 'Personal Information Creation', 'Added Personal Details ', 'far fa-project-diagram text-success', 'Job Seeker Application Details', '2022/02/21 19:42:03');

-- --------------------------------------------------------

--
-- Table structure for table `all_evidence_document`
--

CREATE TABLE `all_evidence_document` (
  `id` int(6) NOT NULL,
  `reference_no` varchar(100) DEFAULT NULL,
  `delivery_note_doc` varchar(100) DEFAULT NULL,
  `purchase_order_doc` varchar(100) DEFAULT NULL,
  `invoice_doc` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(6) NOT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Active',
  `sector` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer_name`, `contact`, `email`, `status`, `sector`, `time_recorded`, `recorded_by`) VALUES
(14, 'Kenchic Ltd', '00', '0@gmail.com', 'Active', '3', '2020-08-14 12:35:17', 'BENCO MBUGUA'),
(15, 'Customer-Window', '0712456', 'customer@gmail.com', 'Active', '2', '2020-09-17 06:55:42', 'BENCO MBUGUA'),
(16, 'Customer _ Roofing', '0722', 'customer@gmail.com', 'Active', '8', '2020-09-17 06:56:16', 'BENCO MBUGUA'),
(17, 'Customer_ Trucking', '0700000', 'Samuel@gmail.com', 'Active', '8', '2020-10-27 09:32:27', 'BENCO MBUGUA'),
(18, 'Bata Shoe Company', '0722280280', 'jane.nganga@bata.com', 'Active', '6', '2020-10-27 09:34:13', 'BENCO MBUGUA'),
(19, 'Afrimac Engineering Ltd', '0710823407', 'engineering@afrimac.co.ke', 'Active', '6', '2020-10-27 09:35:26', 'BENCO MBUGUA');

-- --------------------------------------------------------

--
-- Table structure for table `customer_end_delivery`
--

CREATE TABLE `customer_end_delivery` (
  `id` int(6) NOT NULL,
  `end_product_ref` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `invoice_issued_id` varchar(100) DEFAULT NULL,
  `stock_remaining` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending_approval',
  `document` varchar(100) DEFAULT NULL,
  `delivery_note_doc` varchar(100) DEFAULT NULL,
  `purchase_order_doc` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_end_delivery`
--

INSERT INTO `customer_end_delivery` (`id`, `end_product_ref`, `unit_price`, `qtt`, `total`, `payment_type`, `invoice_issued_id`, `stock_remaining`, `status`, `document`, `delivery_note_doc`, `purchase_order_doc`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(1, '14', '340', '10', '3400.00', '', '', '330', 'pending_approval', '', '', '', '07-Dec-2020', '2020-12-07 14:39:39', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `customer_end_returns`
--

CREATE TABLE `customer_end_returns` (
  `id` int(6) NOT NULL,
  `end_product_ref` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `invoice_issued_id` varchar(100) DEFAULT NULL,
  `stock_remaining` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending_approval',
  `reasons` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `delivery_note_doc` varchar(100) DEFAULT NULL,
  `purchase_order_doc` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_sector`
--

CREATE TABLE `customer_sector` (
  `id` int(6) NOT NULL,
  `sector_name` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_sector`
--

INSERT INTO `customer_sector` (`id`, `sector_name`, `time_recorded`, `recorded_by`) VALUES
(1, 'Education', '2020-05-19 12:08:58', 'PETER KARIUKI'),
(2, 'Farming', '2020-05-19 12:08:58', 'PETER KARIUKI'),
(3, 'Poultry', '2020-05-19 12:09:28', 'PETER KARIUKI'),
(4, 'Government', '2020-05-19 12:09:28', 'PETER KARIUKI'),
(5, 'Media', '2020-05-19 12:09:50', NULL),
(6, 'Manufacturing', '2020-05-19 12:09:50', 'PETER KARIUKI'),
(7, 'Transport', '2020-05-19 12:10:12', 'PETER KARIUKI'),
(8, 'Construction', '2020-05-19 12:10:12', NULL),
(9, 'Electricals', '2020-07-15 10:43:55', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_approvers`
--

CREATE TABLE `delivery_approvers` (
  `id` int(6) NOT NULL,
  `delivery_approver` varchar(100) DEFAULT NULL,
  `product_id` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_approvers`
--

INSERT INTO `delivery_approvers` (`id`, `delivery_approver`, `product_id`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(9, 'inventory@panoramaengineering.com', '6', '22-Jul-2020', '2020-07-22 10:40:24', 'PETER CHEGE KARIUKI'),
(10, 'moffat1@panoramaengineering.com', '14', '07-Dec-2020', '2020-12-07 14:39:39', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `directorate_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`, `department_status`, `directorate_id`, `manager_id`, `created_by`, `time_recorded`) VALUES
('DO', 'Director', 'previous', 'CES OFFICE', 'CMA0168', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm'),
('FA', 'Finance and Administration', 'previous', 'DCS', 'CMA0179', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm'),
('ICT', 'Information Communication Technology', 'previous', 'DCS', 'CMA0154', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm'),
('LO', 'Logistics', 'previous', 'DMO', 'CMA0106', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm'),
('PRO', 'Production & Manufacturing', 'previous', 'DCS', 'CMA0123', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm'),
('PROC', 'Procurement', 'previous', 'DCS', 'CMA0239', 'MARO JILLO ABDALLA', '04/15/2019 03:17:27pm');

-- --------------------------------------------------------

--
-- Table structure for table `endproductresources`
--

CREATE TABLE `endproductresources` (
  `resource_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `end_product`
--

CREATE TABLE `end_product` (
  `id` int(6) NOT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `customer_id` varchar(100) DEFAULT NULL,
  `start_date` varchar(100) DEFAULT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `end_product`
--

INSERT INTO `end_product` (`id`, `product_name`, `unit_price`, `qtt`, `total`, `customer_id`, `start_date`, `end_date`, `duration`, `date_recorded`, `time_recorded`, `recorded_by`, `image`) VALUES
(7, 'GI cover [cone] for duct', '4731', '1', '4731.00', '14', '14-Aug-2020', '15-Aug-2020', '1', '14-Aug-2020', '2020-08-14 12:36:53', 'BENCO MBUGUA', NULL),
(8, 'Window', '8835', '12', '106020.00', '15', '10-Sep-2020', '30-Sep-2020', '20', '17-Sep-2020', '2020-09-17 06:58:21', 'BENCO MBUGUA', NULL),
(9, 'Roofing', '321979', '1', '321979.00', '16', '14-Sep-2020', '19-Sep-2020', '5', '17-Sep-2020', '2020-09-17 07:14:19', 'BENCO MBUGUA', NULL),
(10, 'Trucking 150mm', '1750', '130', '227500.00', '17', '26-Oct-2020', '31-Oct-2020', '5', '27-Oct-2020', '2020-10-27 09:36:18', 'BENCO MBUGUA', NULL),
(11, 'Shanks Size  115', '7', '10000', '70000.00', '18', '24-Oct-2020', '31-Oct-2020', '7', '27-Oct-2020', '2020-10-27 09:38:26', 'BENCO MBUGUA', NULL),
(12, 'SS Storange Tank', '213625', '11', '2349875.00', '19', '26-Oct-2020', '26-Dec-2020', '61', '27-Oct-2020', '2020-10-27 09:43:18', 'BENCO MBUGUA', NULL),
(13, 'Test end pro', '400', '10', '4000.00', '14', '08-Dec-2020', '15-Dec-2020', '7', '07-Dec-2020', '2020-12-07 14:37:20', 'PETER CHEGE KARIUKI', NULL),
(14, 'Por', '340', '340', '115600.00', '19', '08-Dec-2020', '22-Dec-2020', '14', '07-Dec-2020', '2020-12-07 14:39:21', 'PETER CHEGE KARIUKI', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_comments`
--

CREATE TABLE `general_comments` (
  `id` int(6) NOT NULL,
  `reference_no` varchar(250) NOT NULL,
  `general_comments` varchar(500) NOT NULL,
  `commentor` varchar(250) NOT NULL,
  `email_of_commentor` varchar(250) NOT NULL,
  `date_commented` varchar(250) NOT NULL,
  `changed` varchar(10) NOT NULL DEFAULT 'no',
  `time_commented` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_comments`
--

INSERT INTO `general_comments` (`id`, `reference_no`, `general_comments`, `commentor`, `email_of_commentor`, `date_commented`, `changed`, `time_commented`) VALUES
(14, 'SPU/R/1', ' this is a general comment', 'MARO JILLO ABDALLA', 'mabdalla@cma.or.ke', '01/02/2019', 'no', '09:08:48am'),
(15, 'SPU/R/2', ' tty', 'Test User SPU', 'spu@testuser.com', '01/09/2019', 'no', '11:41:00am'),
(16, 'SPU/R/1', 'HEllo this is a comment', 'Test User SPU', 'spu@testuser.com', '01/09/2019', 'no', '11:41:51am'),
(17, 'SPU/R/1', 'men', 'MARO JILLO ABDALLA', 'mabdalla@cma.or.ke', '02/08/2019', 'no', '04:12:22pm'),
(18, 'FIN/R/2', ' N.T - National Treasury', 'PETER LEMAIYAN SAIGILU', 'PSaigilu@cma.or.ke', '04/02/2019', 'no', '03:10:02pm');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_issued`
--

CREATE TABLE `invoice_issued` (
  `id` int(6) NOT NULL,
  `invoice_issued_id` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_issued_payment`
--

CREATE TABLE `invoice_issued_payment` (
  `id` int(6) NOT NULL,
  `invoice_issued_id` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `debit` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_received`
--

CREATE TABLE `invoice_received` (
  `id` int(6) NOT NULL,
  `reference_no` int(6) DEFAULT NULL,
  `invoice_received_id` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `stock_order_level` varchar(100) DEFAULT NULL,
  `person_responsible` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `delivery_note_doc` varchar(100) DEFAULT NULL,
  `purchase_order_doc` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_received`
--

INSERT INTO `invoice_received` (`id`, `reference_no`, `invoice_received_id`, `supplier_id`, `unit_price`, `qtt`, `stock_order_level`, `person_responsible`, `total`, `document`, `delivery_note_doc`, `purchase_order_doc`, `date_recorded`, `time_recorded`, `recorded_by`, `payment_type`) VALUES
(17, 1, 'YM2017-166', 'ZHEJIANG YOMIN ELECTRIC LTD', '12450', '217', '100', NULL, '2701650.00', '', '', '', '15-Jul-2020', '2020-07-15 10:52:07', 'PETER CHEGE KARIUKI', '2'),
(18, 2, 'YM2017-166', 'ZHEJIANG YOMIN ELECTRIC LTD', '25500', '124', '100', NULL, '3162000.00', '', '', '', '15-Jul-2020', '2020-07-15 11:00:37', 'PETER CHEGE KARIUKI', '2'),
(19, 3, 'YM2017-166', 'ZHEJIANG YOMIN ELECTRIC LTD', '270', '550', '100', NULL, '148500.00', '', '', '', '15-Jul-2020', '2020-07-15 11:02:38', 'PETER CHEGE KARIUKI', '2'),
(20, 4, 'CX2015072501', 'CHUANG XIONG GROUP TECHNOLOGY LTD', '228', '9550', '1000', NULL, '2177400.00', '', '', '', '15-Jul-2020', '2020-07-15 11:06:20', 'PETER CHEGE KARIUKI', '2'),
(21, 5, 'CX2015072501', 'CHUANG XIONG GROUP TECHNOLOGY LTD', '278', '11213', '1000', NULL, '3117214.00', '', '', '', '15-Jul-2020', '2020-07-15 11:07:19', 'PETER CHEGE KARIUKI', '2'),
(22, 6, 'CX2015072501', 'CHUANG XIONG GROUP TECHNOLOGY LTD', '18', '6672', '1000', NULL, '120096.00', '', '', '', '15-Jul-2020', '2020-07-15 11:25:47', 'PETER CHEGE KARIUKI', '2'),
(23, 7, 'CX2015072501', 'CHUANG XIONG GROUP TECHNOLOGY LTD', '20', '32161', '1000', NULL, '643220.00', '', '', '', '15-Jul-2020', '2020-07-15 11:26:39', 'PETER CHEGE KARIUKI', '2'),
(24, 8, '1407422', 'Central Auto & Hardware Ltd', '3200', '1', '1', NULL, '3200.00', '', '', '', '14-Aug-2020', '2020-08-14 12:32:52', 'BENCO MBUGUA', '3'),
(25, 9, '98431', 'Nail N Steels Ltd', '680', '29', '1', NULL, '19720.00', '', '', '', '25-Aug-2020', '2020-08-25 09:27:36', 'BENCO MBUGUA', '2'),
(26, 10, '98431', 'Nail N Steels Ltd', '250', '13', '1', NULL, '3250.00', '', '', '', '25-Aug-2020', '2020-08-25 09:28:46', 'BENCO MBUGUA', '2'),
(27, 11, '1407422', 'Central Auto & Hardware Ltd', '3200', '1', '1', NULL, '3200.00', '', '', '', '25-Aug-2020', '2020-08-25 09:30:37', 'BENCO MBUGUA', '3'),
(28, 12, '288521', 'Kens Metal Industries Ltd', '129', '3', '1', NULL, '387.00', '', '', '', '25-Aug-2020', '2020-08-25 09:35:50', 'BENCO MBUGUA', '2'),
(29, 13, '288980', 'Kens Metal Industries Ltd', '467', '2', '1', NULL, '934.00', '', '', '', '25-Aug-2020', '2020-08-25 09:39:10', 'BENCO MBUGUA', '2'),
(30, 14, '97', 'P&L Above Heights Ltd', '211', '300', '1', NULL, '63300.00', '', '', '', '25-Aug-2020', '2020-08-25 09:42:52', 'BENCO MBUGUA', '2'),
(31, 15, '1409161', 'Central Auto & Hardware Ltd', '1045', '10', '1', NULL, '10450.00', '', '', '', '25-Aug-2020', '2020-08-25 09:44:20', 'BENCO MBUGUA', '3'),
(32, 16, '1409161', 'Central Auto & Hardware Ltd', '913', '10', '1', NULL, '9130.00', '', '', '', '25-Aug-2020', '2020-08-25 09:45:26', 'BENCO MBUGUA', '1'),
(33, 17, '1409161', 'Central Auto & Hardware Ltd', '1395', '1', '1', NULL, '1395.00', '', '', '', '25-Aug-2020', '2020-08-25 09:46:24', 'BENCO MBUGUA', '1'),
(34, 18, '1409161', 'Central Auto & Hardware Ltd', '60', '47', '1', NULL, '2820.00', '', '', '', '25-Aug-2020', '2020-08-25 09:47:29', 'BENCO MBUGUA', '1'),
(35, 19, '1409161', 'Central Auto & Hardware Ltd', '375', '21', '1', NULL, '7875.00', '', '', '', '25-Aug-2020', '2020-08-25 09:48:53', 'BENCO MBUGUA', '1'),
(36, 20, '1411181', 'Central Auto & Hardware Ltd', '1395', '3', '1', NULL, '4185.00', '', '', '', '28-Aug-2020', '2020-08-28 08:49:12', 'BENCO MBUGUA', '1'),
(37, 21, '1411181', 'Central Auto & Hardware Ltd', '1139', '1', '1', NULL, '1139.00', '', '', '', '28-Aug-2020', '2020-08-28 08:50:07', 'BENCO MBUGUA', '1'),
(38, 22, '1411181', 'Central Auto & Hardware Ltd', '495', '6', '1', NULL, '2970.00', '', '', '', '28-Aug-2020', '2020-08-28 08:50:47', 'BENCO MBUGUA', '1'),
(39, 23, '1411181', 'Central Auto & Hardware Ltd', '3600', '1', '1', NULL, '3600.00', '', '', '', '28-Aug-2020', '2020-08-28 08:51:32', 'BENCO MBUGUA', '1'),
(40, 24, '162078', 'Noble Gases', '1220', '1', '1', NULL, '1220.00', '', '', '', '28-Aug-2020', '2020-08-28 08:54:23', 'BENCO MBUGUA', '1'),
(41, 25, '700072334', 'Kansai Paint', '13680', '1', '1', NULL, '13680.00', '', '', '', '28-Aug-2020', '2020-08-28 08:59:04', 'BENCO MBUGUA', '1'),
(42, 26, '34138', 'Maisha Steel  Ltd', '350', '5', '1', NULL, '1750.00', '', '', '', '28-Aug-2020', '2020-08-28 11:04:37', 'BENCO MBUGUA', '1'),
(43, 27, '16112', 'Clerb Enterprises Ltd', '1700', '2', '1', NULL, '3400.00', '', '', '', '28-Aug-2020', '2020-08-28 11:08:58', 'BENCO MBUGUA', '1'),
(44, 28, '98990', 'Nail N Steels Ltd', '3250', '40', '40', NULL, '130000.00', '', '', '', '17-Sep-2020', '2020-09-17 06:22:09', 'BENCO MBUGUA', '2'),
(45, 29, '98936', 'Nail N Steels Ltd', '370', '15', '1', NULL, '5550.00', '', '', '', '17-Sep-2020', '2020-09-17 06:23:46', 'BENCO MBUGUA', '2'),
(46, 30, '98936', 'Nail N Steels Ltd', '250', '11', '1', NULL, '2750.00', '', '', '', '17-Sep-2020', '2020-09-17 06:24:33', 'BENCO MBUGUA', '2'),
(47, 31, '1414921', 'Central Auto & Hardware Ltd', '13305', '3', '1', NULL, '39915.00', '', '', '', '17-Sep-2020', '2020-09-17 06:26:05', 'BENCO MBUGUA', '1'),
(48, 32, '1414921', 'Central Auto & Hardware Ltd', '4045', '3', '1', NULL, '12135.00', '', '', '', '17-Sep-2020', '2020-09-17 06:34:00', 'BENCO MBUGUA', '1'),
(49, 33, '1414921', 'Central Auto & Hardware Ltd', '770', '2', '2', NULL, '1540.00', '', '', '', '17-Sep-2020', '2020-09-17 06:35:00', 'BENCO MBUGUA', '1'),
(50, 34, '1414921', 'Central Auto & Hardware Ltd', '2385', '12', '1', NULL, '28620.00', '', '', '', '17-Sep-2020', '2020-09-17 06:35:51', 'BENCO MBUGUA', '1'),
(51, 35, '1414921', 'Central Auto & Hardware Ltd', '684', '10', '1', NULL, '6840.00', '', '', '', '17-Sep-2020', '2020-09-17 06:37:20', 'BENCO MBUGUA', '1'),
(52, 36, '15413', 'Maisha Steel  Ltd', '1450', '14', '1', NULL, '20300.00', '', '', '', '17-Sep-2020', '2020-09-17 07:03:29', 'BENCO MBUGUA', '1'),
(53, 37, '66242', 'Orbital Fastner', '300', '1', '1', NULL, '300.00', '', '', '', '17-Sep-2020', '2020-09-17 07:08:13', 'BENCO MBUGUA', '1'),
(54, 38, '101', 'P carlos hardware', '3', '150', '1', NULL, '450.00', '', '', '', '17-Sep-2020', '2020-09-17 07:09:20', 'BENCO MBUGUA', '1'),
(55, 39, '101', 'P carlos hardware', '80', '50', '1', NULL, '4000.00', '', '', '', '17-Sep-2020', '2020-09-17 07:10:20', 'BENCO MBUGUA', '1'),
(56, 40, '101', 'P carlos hardware', '5', '32', '1', NULL, '160.00', '', '', '', '17-Sep-2020', '2020-09-17 07:11:07', 'BENCO MBUGUA', '1'),
(57, 41, '101', 'P carlos hardware', '60', '32', '1', NULL, '1920.00', '', '', '', '17-Sep-2020', '2020-09-17 07:11:45', 'BENCO MBUGUA', '1'),
(58, 42, '734', 'GYM  Car Auto Paints', '1600', '2', '1', NULL, '3200.00', '', '', '', '17-Sep-2020', '2020-09-17 07:23:13', 'BENCO MBUGUA', '1'),
(59, 43, '734', 'GYM  Car Auto Paints', '600', '2', '1', NULL, '1200.00', '', '', '', '17-Sep-2020', '2020-09-17 07:23:53', 'BENCO MBUGUA', '1'),
(60, 44, '1419626', 'Central Auto & Hardware Ltd', '2750', '3', '1', NULL, '8250.00', '', '', '', '07-Oct-2020', '2020-10-07 07:37:59', 'BENCO MBUGUA', '1'),
(61, 45, '163431', 'Noble Gases', '9745', '1', '1', NULL, '9745.00', '', '', '', '07-Oct-2020', '2020-10-07 07:38:55', 'BENCO MBUGUA', '1'),
(62, 46, '99584', 'Nail N Steels Ltd', '1080', '23', '1', NULL, '24840.00', '', '', '', '07-Oct-2020', '2020-10-07 07:40:06', 'BENCO MBUGUA', '2'),
(63, 47, '903396166', 'Akzonobel', '17100', '1', '1', NULL, '17100.00', '', '', '', '07-Oct-2020', '2020-10-07 07:44:11', 'BENCO MBUGUA', '1'),
(64, 48, '327774', 'Kens Metal Industries Ltd', '957', '1', '1', NULL, '957.00', '', '', '', '07-Oct-2020', '2020-10-07 07:46:11', 'BENCO MBUGUA', '1'),
(65, 49, '327774', 'Kens Metal Industries Ltd', '129', '9', '1', NULL, '1161.00', '', '', '', '07-Oct-2020', '2020-10-07 07:47:41', 'BENCO MBUGUA', '1'),
(66, 50, '76187', 'Monross Hardware', '1500', '2', '1', NULL, '3000.00', '', '', '', '07-Oct-2020', '2020-10-07 07:52:05', 'BENCO MBUGUA', '3'),
(67, 51, '76187', 'Monross Hardware', '2000', '1', '1', NULL, '2000.00', '', '', '', '07-Oct-2020', '2020-10-07 07:52:54', 'BENCO MBUGUA', '3'),
(68, 52, '76187', 'Monross Hardware', '550', '1', '1', NULL, '550.00', '', '', '', '07-Oct-2020', '2020-10-07 07:53:33', 'BENCO MBUGUA', '3'),
(69, 53, '76187', 'Monross Hardware', '980', '10', '1', NULL, '9800.00', '', '', '', '07-Oct-2020', '2020-10-07 07:54:15', 'BENCO MBUGUA', '3'),
(70, 54, '11330249', 'APEX STEEL LTD', '16000', '3', '1', NULL, '48000.00', '', '', '', '21-Oct-2020', '2020-10-21 11:54:30', 'BENCO MBUGUA', '2'),
(71, 55, '11330249', 'APEX STEEL LTD', '30499', '13', '13', NULL, '396487.00', '', '', '', '21-Oct-2020', '2020-10-21 11:55:34', 'BENCO MBUGUA', '2'),
(72, 56, '11330249', 'APEX STEEL LTD', '8325', '3', '3', NULL, '24975.00', '', '', '', '21-Oct-2020', '2020-10-21 11:56:20', 'BENCO MBUGUA', '2'),
(73, 57, '11330249', 'APEX STEEL LTD', '12599', '1', '1', NULL, '12599.00', '', '', '', '21-Oct-2020', '2020-10-21 11:57:13', 'BENCO MBUGUA', '2'),
(74, 58, '11330249', 'APEX STEEL LTD', '3200', '5', '1', NULL, '16000.00', '', '', '', '21-Oct-2020', '2020-10-21 11:57:51', 'BENCO MBUGUA', '2'),
(75, 59, '1424821', 'Central Auto & Hardware Ltd', '2750', '30', '1', NULL, '82500.00', '', '', '', '27-Oct-2020', '2020-10-27 09:28:51', 'BENCO MBUGUA', '1'),
(76, 60, '1424821', 'Central Auto & Hardware Ltd', '6065', '1', '1', NULL, '6065.00', '', '', '', '27-Oct-2020', '2020-10-27 09:29:46', 'BENCO MBUGUA', '1'),
(77, 61, '292520', 'Kens Metal Industries Ltd', '607', '1', '1', NULL, '607.00', '', '', '', '28-Oct-2020', '2020-10-28 08:39:02', 'BENCO MBUGUA', '2'),
(78, 62, '292471', 'Kens Metal Industries Ltd', '21147', '1', '1', NULL, '21147.00', '', '', '', '28-Oct-2020', '2020-10-28 08:40:21', 'BENCO MBUGUA', '2'),
(79, 63, '292471', 'Kens Metal Industries Ltd', '3329', '4', '1', NULL, '13316.00', '', '', '', '28-Oct-2020', '2020-10-28 08:41:39', 'BENCO MBUGUA', '2'),
(80, 64, '292471', 'Kens Metal Industries Ltd', '479', '108', '1', NULL, '51732.00', '', '', '', '28-Oct-2020', '2020-10-28 08:42:38', 'BENCO MBUGUA', '2'),
(81, 65, '3239', 'Nasib Industries Ltd', '650', '20', '1', NULL, '13000.00', '', '', '', '28-Oct-2020', '2020-10-28 08:47:14', 'BENCO MBUGUA', '1'),
(82, 66, '950', 'Fujjo Hardware', '4500', '1', '1', NULL, '4500.00', '', '', '', '29-Oct-2020', '2020-10-29 06:43:12', 'BENCO MBUGUA', '1'),
(83, 67, '950', 'Fujjo Hardware', '4500', '1', '1', NULL, '4500.00', '', '', '', '29-Oct-2020', '2020-10-29 06:43:56', 'BENCO MBUGUA', '1'),
(84, 67, '950', 'Fujjo Hardware', '4500', '1', '1', NULL, '4500.00', '', '', '', '29-Oct-2020', '2020-10-29 06:44:08', 'BENCO MBUGUA', '1'),
(85, 67, '950', 'Fujjo Hardware', '4500', '1', '1', NULL, '4500.00', '', '', '', '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA', '1'),
(86, 67, '950', 'Fujjo Hardware', '4500', '1', '1', NULL, '4500.00', '', '', '', '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA', '1'),
(87, 68, '950', 'Fujjo Hardware', '200', '10', '1', NULL, '2000.00', '', '', '', '29-Oct-2020', '2020-10-29 06:45:23', 'BENCO MBUGUA', '1'),
(88, 69, '292587', 'Kens Metal Industries Ltd', '5130', '1', '1', NULL, '5130.00', '', '', '', '29-Oct-2020', '2020-10-29 06:47:08', 'BENCO MBUGUA', '2'),
(89, 70, '292719', 'Kens Metal Industries Ltd', '4503', '9', '1', NULL, '40527.00', '', '', '', '04-Nov-2020', '2020-11-04 09:05:17', 'BENCO MBUGUA', '2'),
(90, 71, '1426129', 'Central Auto & Hardware Ltd', '2750', '36', '1', NULL, '99000.00', '', '', '', '04-Nov-2020', '2020-11-04 09:06:56', 'BENCO MBUGUA', '1'),
(91, 72, '164638', 'Noble Gases', '9745', '1', '1', NULL, '9745.00', '', '', '', '04-Nov-2020', '2020-11-04 09:08:48', 'BENCO MBUGUA', '1'),
(92, 73, '292804', 'Kens Metal Industries Ltd', '593', '6', '1', NULL, '3558.00', '', '', '', '04-Nov-2020', '2020-11-04 09:11:01', 'BENCO MBUGUA', '2'),
(93, 74, '292804', 'Kens Metal Industries Ltd', '3511', '7', '1', NULL, '24577.00', '', '', '', '04-Nov-2020', '2020-11-04 09:12:08', 'BENCO MBUGUA', '2'),
(94, 75, '292804', 'Kens Metal Industries Ltd', '171', '30', '1', NULL, '5130.00', '', '', '', '04-Nov-2020', '2020-11-04 09:13:13', 'BENCO MBUGUA', '2'),
(95, 76, '292804', 'Kens Metal Industries Ltd', '581', '50', '1', NULL, '29050.00', '', '', '', '04-Nov-2020', '2020-11-04 09:14:01', 'BENCO MBUGUA', '2'),
(96, 77, '953', 'Fujjo Hardware', '200', '3', '1', NULL, '600.00', '', '', '', '04-Nov-2020', '2020-11-04 09:16:40', 'BENCO MBUGUA', '1'),
(97, 78, '953', 'Fujjo Hardware', '100', '7', '1', NULL, '700.00', '', '', '', '04-Nov-2020', '2020-11-04 09:17:29', 'BENCO MBUGUA', '1'),
(98, 79, '953', 'Fujjo Hardware', '122', '10', '1', NULL, '1220.00', '', '', '', '04-Nov-2020', '2020-11-04 09:18:24', 'BENCO MBUGUA', '1'),
(99, 80, '1', 'Jumbo Hardware', '3000', '2', '1', NULL, '6000.00', '', '', '', '04-Nov-2020', '2020-11-04 09:24:35', 'BENCO MBUGUA', '1'),
(100, 81, '1', 'Jumbo Hardware', '1000', '3', '1', NULL, '3000.00', '', '', '', '04-Nov-2020', '2020-11-04 09:43:53', 'BENCO MBUGUA', '1'),
(101, 82, 'Tshsbsb', 'Nail N Steels Ltd', '100', '5', '2', NULL, '500.00', '', '', '', '14-Feb-2222', '2022-02-14 12:43:38', 'PETER CHEGE KARIUKI', '1');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_received_payment`
--

CREATE TABLE `invoice_received_payment` (
  `id` int(6) NOT NULL,
  `invoice_received_id` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `debit` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_received_payment`
--

INSERT INTO `invoice_received_payment` (`id`, `invoice_received_id`, `supplier_id`, `transaction_id`, `payment_type`, `debit`, `date_recorded`, `time_recorded`, `recorded_by`, `document`) VALUES
(1, '98', '', 'tryinv', '2', '45000', '17-Aug-2121', '2021-08-17 05:23:58', 'PETER CHEGE KARIUKI', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_payments`
--

CREATE TABLE `mpesa_payments` (
  `id` int(11) NOT NULL,
  `TransactionType` varchar(200) DEFAULT NULL,
  `TransID` varchar(200) DEFAULT NULL,
  `TransTime` varchar(200) DEFAULT NULL,
  `TransAmount` varchar(200) DEFAULT NULL,
  `BusinessShortCode` varchar(200) DEFAULT NULL,
  `BillRefNumber` varchar(200) DEFAULT NULL,
  `InvoiceNumber` varchar(200) DEFAULT NULL,
  `MSISDN` varchar(200) DEFAULT NULL,
  `FirstName` varchar(200) DEFAULT NULL,
  `MiddleName` varchar(200) DEFAULT NULL,
  `LastName` varchar(200) DEFAULT NULL,
  `OrgAccountBalance` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page_requests`
--

CREATE TABLE `page_requests` (
  `id` int(6) NOT NULL,
  `page_id` varchar(500) DEFAULT NULL,
  `page_name` varchar(500) NOT NULL,
  `requested_by` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL DEFAULT 'default',
  `time_requested` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_requests`
--

INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(686, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-15 10:28:27.081722'),
(687, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:28:27.851345'),
(688, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-15 10:34:37.131249'),
(689, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:34:39.389083'),
(690, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:36:40.839742'),
(691, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:36:45.996428'),
(692, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:37:18.902710'),
(693, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:44:01.277768'),
(694, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:45:27.615392'),
(695, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:48:33.568373'),
(696, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:48:40.762232'),
(697, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 10:52:08.747845'),
(698, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:00:39.956678'),
(699, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:02:40.299249'),
(700, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:06:22.346083'),
(701, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:07:20.535898'),
(702, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-15 11:24:32.582242'),
(703, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:24:35.070619'),
(704, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:25:49.567489'),
(705, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:26:42.327617'),
(706, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:22.200511'),
(707, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:30.405377'),
(708, 'stock-management-link', '\n                      \n                      Item List\n                    7 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:35.477484'),
(709, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:37.207749'),
(710, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:45.920468'),
(711, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:47.145974'),
(712, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:35:49.846199'),
(713, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-15 11:42:21.188375'),
(714, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-07-15 11:51:35.455227'),
(715, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:22:29.701238'),
(716, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:25:07.958685'),
(717, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:25:09.548076'),
(718, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:25:16.638033'),
(719, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:25:28.145513'),
(720, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-07-15 12:27:14.066026'),
(721, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:11.730448'),
(722, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:12.447453'),
(723, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:21.210580'),
(724, 'open-total-projects-modal', 'Dashboard Total Stocks', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:33.583591'),
(725, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:38.677725'),
(726, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'mbugua@panoramaengineering.com', '', '2020-07-15 12:39:46.822184'),
(727, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:40:18.920020'),
(728, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:40:21.705499'),
(729, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:42:26.275809'),
(730, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:42:28.467233'),
(731, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:42:33.176532'),
(732, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:49:05.494031'),
(733, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:49:07.502698'),
(734, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:49:19.590257'),
(735, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:49:27.495734'),
(736, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:49:38.745561'),
(737, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:50:32.914942'),
(738, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:50:35.003710'),
(739, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:51:44.012408'),
(740, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:51:45.793538'),
(741, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:52:16.634836'),
(742, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:52:26.624440'),
(743, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:52:30.998536'),
(744, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:52:53.327210'),
(745, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:52:56.259188'),
(746, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:53:03.895322'),
(747, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:53:19.994105'),
(748, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:53:25.550924'),
(749, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:53:59.632407'),
(750, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:54:31.216533'),
(751, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-15 12:55:05.491562'),
(752, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:55:12.379112'),
(753, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:57:38.068380'),
(754, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:57:47.937690'),
(755, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 12:57:49.458051'),
(756, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:59:05.504160'),
(757, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 12:59:10.562953'),
(758, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 13:00:01.271452'),
(759, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 13:00:07.449871'),
(760, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-07-15 13:01:09.062767'),
(761, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-15 13:01:10.310757'),
(762, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 13:01:13.099993'),
(763, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-15 13:02:11.915130'),
(764, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-15 13:02:21.075167'),
(765, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-16 07:23:34.928839'),
(766, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 07:23:35.578538'),
(767, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 07:24:12.012452'),
(768, 'mail_logs_tab', 'Mail Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 07:24:27.200024'),
(769, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-16 08:20:08.570259'),
(770, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 08:20:09.247072'),
(771, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 08:20:16.116512'),
(772, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-16 17:37:13.089985'),
(773, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-16 17:37:15.201045'),
(774, 'Inventory-management-module', 'Inventory Management Module', 'osino@panoramaengineering.com', '', '2020-07-17 12:43:31.030776'),
(775, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'osino@panoramaengineering.com', '', '2020-07-17 12:43:31.786132'),
(776, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'osino@panoramaengineering.com', '', '2020-07-17 12:44:46.845056'),
(777, 'end-product-resource-link', '\n                     \n                     Resource Planning\n                   ', 'osino@panoramaengineering.com', '', '2020-07-17 12:44:52.635889'),
(778, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'osino@panoramaengineering.com', '', '2020-07-17 12:44:59.402220'),
(779, 'reports-link', 'Reports all_profit_loss', 'osino@panoramaengineering.com', '', '2020-07-17 12:45:03.941788'),
(780, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'osino@panoramaengineering.com', '', '2020-07-17 12:45:08.254762'),
(781, 'customer-ledger-management-link', '\n                      \n                      Customer Ledger\n                    ', 'osino@panoramaengineering.com', '', '2020-07-17 12:45:58.378650'),
(782, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'osino@panoramaengineering.com', '', '2020-07-17 12:46:03.853390'),
(783, 'user-profile-link', 'User Profile', 'osino@panoramaengineering.com', '', '2020-07-17 12:46:36.933351'),
(784, 'Inventory-management-module', 'Inventory Management Module', 'osino@panoramaengineering.com', '', '2020-07-17 12:52:41.186022'),
(785, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'osino@panoramaengineering.com', '', '2020-07-17 12:52:42.228200'),
(786, 'stock-management-link', '\n                      \n                      Item List\n                    1 | PANORAMA ', 'osino@panoramaengineering.com', '', '2020-07-17 13:01:43.634956'),
(787, 'stock-list-payments-tab', '\n             Payments\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:01:44.584154'),
(788, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:08.281775'),
(789, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:10.700055'),
(790, 'stock-list-payments-tab', '\n             Payments\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:17.938572'),
(791, 'end-product-tab', '\n            Stocks Request\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:23.319168'),
(792, 'stock-list-payments-tab', '\n             Payments\n        ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:29.388930'),
(793, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:38.332362'),
(794, 'Inventory-management-module', 'Inventory Management Module', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:50.587349'),
(795, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'osino@panoramaengineering.com', '', '2020-07-17 13:02:52.843772'),
(796, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'osino@panoramaengineering.com', '', '2020-07-17 13:03:02.263002'),
(797, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'osino@panoramaengineering.com', '', '2020-07-17 13:03:42.838752'),
(798, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-17 16:53:36.401789'),
(799, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-17 16:53:43.994497'),
(800, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-17 16:59:57.105352'),
(801, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-17 17:00:04.706420'),
(802, 'page_logs_tab', 'Page Logs', 'inventory@panoramaengineering.com', '', '2020-07-17 17:00:47.341353'),
(803, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-17 17:01:39.959110'),
(804, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-07-17 17:01:57.101722'),
(805, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2020-07-17 17:02:51.726929'),
(806, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-17 17:02:57.906149'),
(807, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-17 17:04:06.206457'),
(808, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:06.590973'),
(809, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:26.852603'),
(810, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:28.815456'),
(811, 'stock-management-link', '\n                      \n                      Item List\n                    7 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:44.105426'),
(812, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:44.674607'),
(813, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:00:59.721253'),
(814, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:01:01.096154'),
(815, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:01:05.001908'),
(816, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:01:07.135247'),
(817, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2020-07-19 15:01:15.899070'),
(818, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-20 10:34:11.833792'),
(819, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-20 10:34:14.011627'),
(820, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-07-20 10:34:24.410973'),
(821, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:05.504644'),
(822, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:06.301317'),
(823, 'page_logs_tab', 'Page Logs', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:11.543526'),
(824, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:15.031533'),
(825, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:29.196907'),
(826, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:30.146415'),
(827, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:42.568269'),
(828, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:16:51.582302'),
(829, 'stock-management-link', '\n                      \n                      Item List\n                    5 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:19:20.083270'),
(830, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:19:20.699854'),
(831, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 09:19:27.888783'),
(832, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-22 10:32:16.228573'),
(833, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:32:17.142279'),
(834, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:32:22.577296'),
(835, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:37:48.495515'),
(836, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:38:27.735996'),
(837, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:39:06.401099'),
(838, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:39:34.101716'),
(839, 'end-product-management-link', '\n                     \n                     End Product List\n                   6 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:39:43.045166'),
(840, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:39:43.966219'),
(841, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:40:00.247952'),
(842, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:40:25.926541'),
(843, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:40:33.136174'),
(844, 'open-dashboard-project-payments-modal', 'Dashboard Deliveries ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:40:37.509294'),
(845, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:41:12.219388'),
(846, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:43:47.540280'),
(847, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:43:57.548931'),
(848, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:44:04.084148'),
(849, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:44:28.260225'),
(850, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-22 10:55:39.513328'),
(851, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-22 10:55:40.239575'),
(852, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-07-22 10:55:48.004062'),
(853, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:34.211692'),
(854, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:35.884113'),
(855, 'stock-management-link', '\n                      \n                      Item List\n                    7 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:43.387716'),
(856, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:44.458940'),
(857, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:50.322222'),
(858, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:54.674454'),
(859, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:22:57.834153'),
(860, 'end-product-resource-link', '\n                     \n                     Resource Planning\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:23:26.406540'),
(861, 'end-product-resource-link', '\n                     \n                     Resource Planning\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:23:35.435789'),
(862, 'admin-user-management-link', '\n                     \n                     Resource Sheet\n                   ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:23:39.292045'),
(863, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-07-23 11:33:12.861138'),
(864, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-07-23 11:33:18.729604'),
(865, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-03 11:53:35.933111'),
(866, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-03 11:53:36.816895'),
(867, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-08-03 11:53:45.755179'),
(868, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-04 12:26:40.548661'),
(869, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:26:42.604487'),
(870, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-04 12:26:56.277271'),
(871, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:26:56.955208'),
(872, 'stock-management-link', '\n                      \n                      Item List\n                    7 | PANORAMA ', 'danson@panoramaengineering.com', '', '2020-08-04 12:27:07.437732'),
(873, 'stock-list-payments-tab', '\n             Payments\n        ', 'danson@panoramaengineering.com', '', '2020-08-04 12:27:08.039917'),
(874, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'danson@panoramaengineering.com', '', '2020-08-04 12:27:16.799209'),
(875, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:27:31.552486'),
(876, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:27:42.644088'),
(877, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-04 12:28:04.432156'),
(878, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-04 12:28:07.858321'),
(879, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-04 12:28:26.789047'),
(880, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-04 12:28:31.378166'),
(881, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:28:55.198547'),
(882, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-04 12:29:07.940118'),
(883, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-04 12:29:10.544463'),
(884, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:34:53.467522'),
(885, 'stock-management-link', '\n                      \n                      Item List\n                    7 | PANORAMA ', 'danson@panoramaengineering.com', '', '2020-08-04 12:34:55.930095'),
(886, 'stock-list-payments-tab', '\n             Payments\n        ', 'danson@panoramaengineering.com', '', '2020-08-04 12:34:56.276100'),
(887, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-04 12:37:18.482260'),
(888, 'reports-link', 'Reports all_profit_loss', 'danson@panoramaengineering.com', '', '2020-08-04 12:37:27.112868'),
(889, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-04 12:37:52.943256'),
(890, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-04 12:40:20.196155'),
(891, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:43:04.184192'),
(892, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:44:22.157651'),
(893, 'stock-category-management-link', '\n                      \n                      Category\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:44:39.281529'),
(894, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-04 12:44:47.870229'),
(895, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'danson@panoramaengineering.com', '', '2020-08-04 12:45:25.343800'),
(896, 'activity_logs_tab', 'Activity Logs', 'danson@panoramaengineering.com', '', '2020-08-04 12:45:25.694879'),
(897, 'sign_in_logs_tab', 'Sign in Logs', 'danson@panoramaengineering.com', '', '2020-08-04 12:45:34.277398'),
(898, 'user-profile-link', 'User Profile', 'danson@panoramaengineering.com', '', '2020-08-04 12:46:03.167244'),
(899, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'danson@panoramaengineering.com', '', '2020-08-04 12:47:52.060411'),
(900, 'activity_logs_tab', 'Activity Logs', 'danson@panoramaengineering.com', '', '2020-08-04 12:47:52.386924'),
(901, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-04 12:48:37.492275'),
(902, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'danson@panoramaengineering.com', '', '2020-08-04 12:48:42.575315'),
(903, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'danson@panoramaengineering.com', '', '2020-08-04 12:48:52.126151'),
(904, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-04 12:50:12.572928'),
(905, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Termination Kits                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:26:39\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-04 12:50:13.384123'),
(906, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-04 12:50:14.607173'),
(907, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:50:28.406157'),
(908, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:50:28.855576'),
(909, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-04 12:54:52.191195'),
(910, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Termination Kits                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:26:39\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-04 12:54:53.111853'),
(911, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-04 12:54:55.497924'),
(912, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:13.920050'),
(913, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:36.382636'),
(914, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:36.969453'),
(915, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:37.175567'),
(916, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:37.527006'),
(917, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:38.279515'),
(918, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-04 12:55:38.387101'),
(919, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-06 13:10:45.984381'),
(920, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:10:47.217043'),
(921, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-06 13:12:03.292438'),
(922, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-06 13:13:22.363558'),
(923, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-06 13:14:20.075349'),
(924, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-06 13:14:41.394527'),
(925, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-06 13:16:43.753033'),
(926, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-06 13:17:01.608931'),
(927, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-06 13:18:41.450737'),
(928, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Termination Kits                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:25:47\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-06 13:18:41.881078'),
(929, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-06 13:18:42.819923'),
(930, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:18:49.546799'),
(931, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:18:49.661004'),
(932, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-06 13:19:05.348026'),
(933, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:12.607065'),
(934, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:12.807696'),
(935, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:13.365249'),
(936, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:13.560912'),
(937, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:13.687807');
INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(938, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:13.704624'),
(939, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:20.956407'),
(940, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:33.344755'),
(941, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:33.389938'),
(942, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:33.405045'),
(943, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:02:38\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:34.028108'),
(944, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:02:38\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:34.584027'),
(945, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:02:38\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:34.598943'),
(946, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:35.462811'),
(947, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:40.298995'),
(948, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:40.435477'),
(949, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-06 13:20:51.068697'),
(950, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:22:21.341077'),
(951, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:22:21.790104'),
(952, 'supplier-ledger-management-link', '\n                     \n                     Supplier Ledger\n                   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:23:31.388240'),
(953, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:23:54.387224'),
(954, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 04:52:07\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:23:55.156468'),
(955, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:23:56.631949'),
(956, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:24:10.802028'),
(957, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:24:10.808377'),
(958, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-10 08:29:08.918018'),
(959, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:01.296451'),
(960, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:01.567160'),
(961, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:01.789125'),
(962, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:01.870981'),
(963, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:01.984653'),
(964, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:02.118914'),
(965, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:14.770394'),
(966, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:14.809708'),
(967, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:14.829258'),
(968, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug  compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:00:37\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:15.403380'),
(969, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug  compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:00:37\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:15.480020'),
(970, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Copper lug  compress                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:00:37\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:15.524152'),
(971, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:15.925445'),
(972, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:19.394462'),
(973, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:19.421783'),
(974, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.668405'),
(975, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.675772'),
(976, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.683321'),
(977, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.690310'),
(978, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.805169'),
(979, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:34.992775'),
(980, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:35.139243'),
(981, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:35.298857'),
(982, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:35.318753'),
(983, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:35.392836'),
(984, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:36.532479'),
(985, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:51.954762'),
(986, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:52.337113'),
(987, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:52.472040'),
(988, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:52.482800'),
(989, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:30:56.251230'),
(990, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:02.925418'),
(991, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Expulsion Fuse-cut-out                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:06:20\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:03.753512'),
(992, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:04.334786'),
(993, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:15.214526'),
(994, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:29.949340'),
(995, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:30.136295'),
(996, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:30.263962'),
(997, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:30.298761'),
(998, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:30.319612'),
(999, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:30.337893'),
(1000, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:39.565953'),
(1001, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:39.626733'),
(1002, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:39.633293'),
(1003, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Expulsion Fuse-cut-out                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:06:20\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:40.156061'),
(1004, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Expulsion Fuse-cut-out                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:06:20\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:40.214119'),
(1005, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Expulsion Fuse-cut-out                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-07-15 05:06:20\n                      \n                    \n                  ', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:40.325249'),
(1006, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:40.634722'),
(1007, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:40.912150'),
(1008, 'pending_approval_tab', ' New Stocks Pending Approval', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:45.935410'),
(1009, 'approved_stocks_tab', 'Approved Stocks', 'danson@panoramaengineering.com', '', '2020-08-10 08:31:52.129725'),
(1010, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:17.571274'),
(1011, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.007713'),
(1012, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.133093'),
(1013, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.156092'),
(1014, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.170416'),
(1015, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.177481'),
(1016, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.308527'),
(1017, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.444831'),
(1018, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.476276'),
(1019, 'pending_approval_tab', '\n\n\n  \n  \n  Panorama | Inventory \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n    \n  \n  \n    \n  \n  \n  \n  \n  \n  \n  \n\n  \n\n  \n  \n  \n\n  \n  \n  \n  \n\n\n\n\n  \n  \n    \n    \n      \n        \n      \n      \n         Home\n      \n\n      \n        \n        \n      \n    \n    \n    \n  \n  \n\n  \n  \n    \n    \n      \n      Panorama Inventory\n    \n\n    \n    \n        \n      \n      \n        \n                      \n            \n        \n        \n          DANSON KARIUKI\n        \n      \n\n      \n   ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.488690'),
(1020, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.511072'),
(1021, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.543185'),
(1022, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.549144'),
(1023, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:18.571303'),
(1024, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.084762'),
(1025, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.091449'),
(1026, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.427083'),
(1027, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.447126'),
(1028, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.610136'),
(1029, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.814524'),
(1030, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'danson@panoramaengineering.com', '', '2020-08-10 08:32:29.833782'),
(1031, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-12 09:38:32.706323'),
(1032, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-12 09:38:33.819437'),
(1033, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-12 09:39:33.540137'),
(1034, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-12 09:39:35.537785'),
(1035, 'admin-user-management-link', '\n                  \n                  User Management\n                ', 'inventory@panoramaengineering.com', '', '2020-08-12 12:11:12.191093'),
(1036, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-12 12:12:46.510672'),
(1037, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-12 12:12:47.577644'),
(1038, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-13 09:11:16.947466'),
(1039, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-13 09:11:17.893013'),
(1040, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-13 09:50:23.442214'),
(1041, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-13 09:50:24.106457'),
(1042, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-08-13 09:55:21.100716'),
(1043, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-13 09:55:28.492052'),
(1044, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-13 09:55:29.089212'),
(1045, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-14 07:00:15.758357'),
(1046, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-14 07:00:22.183816'),
(1047, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-14 07:00:28.860215'),
(1048, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-14 07:00:29.478529'),
(1049, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-14 11:09:44.352977'),
(1050, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 11:09:48.452445'),
(1051, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 11:09:54.760093'),
(1052, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:30:00.131743'),
(1053, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:30:02.314922'),
(1054, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:30:12.026042'),
(1055, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:31:22.352518'),
(1056, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:31:31.080456'),
(1057, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:32:54.313518'),
(1058, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:33:39.748697'),
(1059, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:33:57.804251'),
(1060, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:33:58.788253'),
(1061, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:34:19.759047'),
(1062, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:34:41.408742'),
(1063, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:35:19.261217'),
(1064, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:35:36.196074'),
(1065, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:36:55.558499'),
(1066, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:37:18.509688'),
(1067, 'stock-management-link', '\n                      \n                      Item List\n                    8 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:37:24.257708'),
(1068, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:37:25.137042'),
(1069, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:37:30.135241'),
(1070, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:37:50.751410'),
(1071, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:38:14.969545'),
(1072, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-14 12:38:39.602956'),
(1073, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:06:46.897686'),
(1074, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:06:48.069639'),
(1075, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:07:00.119163'),
(1076, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:07:19.746457'),
(1077, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:07:32.566790'),
(1078, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:07:44.268724'),
(1079, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:07:54.268389'),
(1080, 'reports-link', 'Reports all_products_in_production', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:08:03.331388'),
(1081, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:20:15.079923'),
(1082, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 06:20:16.250085'),
(1083, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-15 08:08:02.812684'),
(1084, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 08:08:07.217743'),
(1085, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-15 09:17:55.993681'),
(1086, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-15 09:17:57.123973'),
(1087, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-18 08:38:48.237127'),
(1088, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-18 08:38:49.250299'),
(1089, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-18 08:39:01.181383'),
(1090, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-18 08:39:01.852175'),
(1091, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-18 11:29:14.018244'),
(1092, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-18 11:29:15.034053'),
(1093, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-08-18 11:29:21.940206'),
(1094, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-08-18 11:29:39.967899'),
(1095, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-08-18 11:29:52.796517'),
(1096, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-20 07:46:19.714433'),
(1097, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-20 07:46:20.564369'),
(1098, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-20 10:42:56.946691');
INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(1099, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:42:58.130011'),
(1100, 'stock-management-link', '\n                      \n                      Item List\n                    8 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:43:17.873427'),
(1101, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:43:18.585898'),
(1102, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:43:21.091067'),
(1103, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:43:23.642078'),
(1104, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-08-20 10:43:30.999256'),
(1105, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-24 12:44:12.351692'),
(1106, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-24 12:44:14.045172'),
(1107, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-08-24 12:44:15.495842'),
(1108, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-24 12:44:22.164399'),
(1109, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-25 06:33:56.592816'),
(1110, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 06:33:58.449702'),
(1111, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:21:55.700949'),
(1112, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:21:58.056654'),
(1113, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:22:31.550944'),
(1114, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:23:53.412342'),
(1115, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:24:47.414168'),
(1116, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:24:55.089192'),
(1117, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:27:39.732113'),
(1118, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:28:49.587347'),
(1119, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:30:40.821881'),
(1120, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:31:39.628382'),
(1121, 'supplier-ledger-management-link', '\n                     \n                     Supplier Ledger\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:33:30.086851'),
(1122, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:33:34.734039'),
(1123, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:33:41.985992'),
(1124, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:35:53.101222'),
(1125, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:39:12.519700'),
(1126, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:39:32.258071'),
(1127, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:41:01.835148'),
(1128, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:41:09.324729'),
(1129, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:42:54.838533'),
(1130, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:44:24.331294'),
(1131, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:45:30.339948'),
(1132, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:46:26.516232'),
(1133, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:47:31.896161'),
(1134, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:48:58.137777'),
(1135, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-25 09:55:12.519449'),
(1136, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-25 10:12:47.428311'),
(1137, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:12:48.279326'),
(1138, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:13:29.098023'),
(1139, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-08-25 10:13:33.049136'),
(1140, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2020-08-25 10:13:46.551785'),
(1141, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:13:59.797026'),
(1142, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:07.632431'),
(1143, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:08.700154'),
(1144, 'stock-management-link', '\n                      \n                      Item List\n                    19 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:11.111304'),
(1145, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:11.661230'),
(1146, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:24.553708'),
(1147, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:26.634195'),
(1148, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:30.156368'),
(1149, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:37.829177'),
(1150, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:14:59.100568'),
(1151, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-25 10:21:50.419423'),
(1152, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:21:51.359936'),
(1153, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-25 10:27:46.458147'),
(1154, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:27:47.203273'),
(1155, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:27:53.343701'),
(1156, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-08-25 10:27:53.964706'),
(1157, 'page_logs_tab', 'Page Logs', 'inventory@panoramaengineering.com', '', '2020-08-25 10:27:59.842952'),
(1158, 'supplier-ledger-management-link', '\n                     \n                     Supplier Ledger\n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 10:28:26.915242'),
(1159, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-25 12:20:19.250074'),
(1160, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 12:20:20.368041'),
(1161, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-08-25 12:20:29.572275'),
(1162, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-26 06:59:47.230365'),
(1163, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-26 06:59:48.062946'),
(1164, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2020-08-26 07:00:05.053286'),
(1165, 'reports-link', 'Reports all_stock_items', 'inventory@panoramaengineering.com', '', '2020-08-26 07:00:09.399358'),
(1166, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-08-26 11:42:34.678775'),
(1167, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-08-26 11:42:35.502123'),
(1168, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:46:14.099332'),
(1169, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:46:15.724171'),
(1170, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:46:28.324065'),
(1171, 'stock-management-link', '\n                      \n                      Item List\n                    17 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:46:56.328943'),
(1172, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:46:59.231352'),
(1173, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:47:29.177198'),
(1174, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:47:31.336763'),
(1175, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:49:16.739180'),
(1176, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:50:10.323358'),
(1177, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:50:50.295101'),
(1178, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:51:35.231787'),
(1179, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:51:56.771129'),
(1180, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:52:54.081904'),
(1181, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:53:02.963984'),
(1182, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:54:26.238271'),
(1183, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:56:06.525240'),
(1184, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:57:29.433279'),
(1185, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:57:34.062256'),
(1186, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 08:59:08.076789'),
(1187, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-08-28 10:57:59.386802'),
(1188, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 10:58:04.637531'),
(1189, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:02:48.471655'),
(1190, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:03:52.571240'),
(1191, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:03:56.976536'),
(1192, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:04:40.430841'),
(1193, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:04:59.855406'),
(1194, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:05:58.615481'),
(1195, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:06:54.239700'),
(1196, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:09:02.473950'),
(1197, '', '', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:09:54.825362'),
(1198, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:15:00.967242'),
(1199, 'reports-link', 'Reports all_stock_items', 'mbugua@panoramaengineering.com', '', '2020-08-28 11:15:10.731425'),
(1200, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-02 06:42:03.605488'),
(1201, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-02 06:42:06.455853'),
(1202, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-02 11:43:04.537804'),
(1203, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-02 11:43:06.462325'),
(1204, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-09-03 12:20:31.803680'),
(1205, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-09-03 12:20:32.731998'),
(1206, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-09-03 12:22:15.616028'),
(1207, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-09-08 13:03:55.799742'),
(1208, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-09-08 13:03:57.645471'),
(1209, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-09-08 14:56:55.773147'),
(1210, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-09-08 14:56:57.078234'),
(1211, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-09-08 14:57:45.001534'),
(1212, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-09-08 14:57:45.278616'),
(1213, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-09-16 12:44:42.420404'),
(1214, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-09-16 12:44:43.588114'),
(1215, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-09-16 12:44:54.318937'),
(1216, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:20:35.137975'),
(1217, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:20:36.542830'),
(1218, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:20:42.323355'),
(1219, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:22:13.665930'),
(1220, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:23:49.262332'),
(1221, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:24:35.585722'),
(1222, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:26:07.120334'),
(1223, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:34:04.037163'),
(1224, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:35:03.252182'),
(1225, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:35:54.387187'),
(1226, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:37:22.951568'),
(1227, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:54:27.315118'),
(1228, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:54:43.003747'),
(1229, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:55:43.712909'),
(1230, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:56:17.970348'),
(1231, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:56:23.120823'),
(1232, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:58:23.755720'),
(1233, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:58:40.434385'),
(1234, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:59:03.024074'),
(1235, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:59:03.925741'),
(1236, 'stock-management-link', '\n                      \n                      Item List\n                    35 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:59:44.108027'),
(1237, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:59:45.251382'),
(1238, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 06:59:50.606729'),
(1239, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:00:15.745482'),
(1240, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:00:35.468232'),
(1241, 'stock-management-link', '\n                      \n                      Item List\n                    30 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:00:47.893401'),
(1242, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:00:48.543930'),
(1243, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:00:50.051115'),
(1244, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:01:09.436276'),
(1245, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:01:18.329293'),
(1246, 'stock-management-link', '\n                      \n                      Item List\n                    34 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:01:21.797899'),
(1247, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:01:22.097077'),
(1248, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:01:23.932045'),
(1249, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:02:28.446813'),
(1250, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:03:31.799544'),
(1251, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:05:25.420442'),
(1252, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:06:20.879126'),
(1253, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:07:15.398657'),
(1254, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:07:26.924751'),
(1255, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:08:16.425500'),
(1256, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:09:22.756456'),
(1257, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:10:22.611268'),
(1258, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:11:10.751239'),
(1259, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:11:48.076521'),
(1260, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:12:26.383200'),
(1261, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:12:40.617104'),
(1262, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:14:21.849168'),
(1263, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:14:46.201527'),
(1264, 'stock-management-link', '\n                      \n                      Item List\n                    38 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:00.786539'),
(1265, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:01.170105'),
(1266, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:13.368890'),
(1267, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:35.462580'),
(1268, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:45.506205'),
(1269, 'stock-management-link', '\n                      \n                      Item List\n                    37 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:48.612304'),
(1270, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:49.117151'),
(1271, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:15:53.463428'),
(1272, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:16.199866'),
(1273, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:20.521339'),
(1274, 'stock-management-link', '\n                      \n                      Item List\n                    39 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:29.092903'),
(1275, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:29.713409'),
(1276, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:33.933209'),
(1277, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:48.612952'),
(1278, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:16:52.883227'),
(1279, 'stock-management-link', '\n                      \n                      Item List\n                    34 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:00.100679'),
(1280, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:01.125132'),
(1281, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:05.289788'),
(1282, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:18.268480'),
(1283, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:22.056663'),
(1284, 'stock-management-link', '\n                      \n                      Item List\n                    33 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:37.601961'),
(1285, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:38.274875'),
(1286, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:40.890825'),
(1287, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:51.271247'),
(1288, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:17:59.496943'),
(1289, 'stock-management-link', '\n                      \n                      Item List\n                    36 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:10.691771'),
(1290, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:12.039683'),
(1291, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:15.224315'),
(1292, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:26.393392'),
(1293, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:35.375971'),
(1294, 'stock-management-link', '\n                      \n                      Item List\n                    32 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:47.726046'),
(1295, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:48.494877'),
(1296, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:19:52.175365'),
(1297, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:00.852511'),
(1298, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:05.059956'),
(1299, 'stock-management-link', '\n                      \n                      Item List\n                    31 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:39.018320'),
(1300, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:39.868524'),
(1301, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:41.537936'),
(1302, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:20:52.854043'),
(1303, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:21:33.630243'),
(1304, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:22:30.807916'),
(1305, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:22:33.545321'),
(1306, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:23:17.007975'),
(1307, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:23:55.526766'),
(1308, 'stock-management-link', '\n                      \n                      Item List\n                    42 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:15.321755'),
(1309, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:16.069309'),
(1310, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:28.133146'),
(1311, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:30.380427'),
(1312, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:40.837873'),
(1313, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:24:48.242940'),
(1314, 'stock-management-link', '\n                      \n                      Item List\n                    43 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:25:02.042223'),
(1315, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:25:02.400951'),
(1316, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:25:04.977726'),
(1317, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:25:15.372307'),
(1318, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-17 07:25:25.354355'),
(1319, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-09-17 07:42:28.419687'),
(1320, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-09-17 07:42:29.621341'),
(1321, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:08:18.428867'),
(1322, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:08:20.280531'),
(1323, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:08:27.082427'),
(1324, 'end-product-management-link', '\n                     \n                     End Product List\n                   9 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:09:15.845275'),
(1325, 'stocks-used-tab', '\n            Stocks Used\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:09:16.183386'),
(1326, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:09:40.757143'),
(1327, 'stock-management-link', '\n                      \n                      Item List\n                    35 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:10:29.032840'),
(1328, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:10:29.712243'),
(1329, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:10:32.933604'),
(1330, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:12.618888'),
(1331, 'stock-management-link', '\n                      \n                      Item List\n                    30 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:25.015683'),
(1332, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:25.376721'),
(1333, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:29.720178'),
(1334, 'stock-management-link', '\n                      \n                      Item List\n                    29 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:44.011867'),
(1335, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:44.702434'),
(1336, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:11:48.620062'),
(1337, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:01.236748'),
(1338, 'stock-management-link', '\n                      \n                      Item List\n                    29 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:09.167481'),
(1339, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:09.488000'),
(1340, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:22.898127'),
(1341, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:28.820543'),
(1342, 'end-product-management-link', '\n                     \n                     End Product List\n                   8 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:51.935038'),
(1343, 'stocks-used-tab', '\n            Stocks Used\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:12:52.554296'),
(1344, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:13:08.501269'),
(1345, 'stock-management-link', '\n                      \n                      Item List\n                    29 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:13:19.739096'),
(1346, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:13:20.960360'),
(1347, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:13:32.507301'),
(1348, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:14:01.809443'),
(1349, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:14:13.693306'),
(1350, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:14:48.071247'),
(1351, 'stock-management-link', '\n                      \n                      Item List\n                    41 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:14:51.279936'),
(1352, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:14:51.652361'),
(1353, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:15:04.631877'),
(1354, 'stock-management-link', '\n                      \n                      Item List\n                    29 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:15:15.498935'),
(1355, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:15:16.135920'),
(1356, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:15:30.594937'),
(1357, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:15:52.406505'),
(1358, 'stock-management-link', '\n                      \n                      Item List\n                    26 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:03.797069'),
(1359, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:04.453828'),
(1360, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:09.797131'),
(1361, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:27.869360'),
(1362, 'end-product-management-link', '\n                     \n                     End Product List\n                   8 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:33.825959'),
(1363, 'stocks-used-tab', '\n            Stocks Used\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 08:16:34.144221'),
(1364, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-09-18 11:44:21.477685'),
(1365, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 11:44:24.591482'),
(1366, 'stock-category-management-link', '\n                      \n                      Category\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:19:18.813040'),
(1367, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:19:28.403975'),
(1368, 'stock-management-link', '\n                      \n                      Item List\n                    29 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:19:47.022330'),
(1369, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:19:47.877652'),
(1370, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:20:03.540775');
INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(1371, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:20:29.546241'),
(1372, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:20:38.196825'),
(1373, 'stock-management-link', '\n                      \n                      Item List\n                    27 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:20:56.926116'),
(1374, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:20:57.783317'),
(1375, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:06.255114'),
(1376, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:21.567475'),
(1377, 'stock-management-link', '\n                      \n                      Item List\n                    28 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:30.688537'),
(1378, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:31.527232'),
(1379, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:36.865136'),
(1380, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-09-18 12:21:54.783021'),
(1381, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2020-10-07 06:30:53.309000'),
(1382, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-10-07 07:17:53.192570'),
(1383, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:17:54.153979'),
(1384, 'stock-management-link', '\n                      \n                      Item List\n                    43 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:18:03.798534'),
(1385, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:18:04.524984'),
(1386, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:18:07.181070'),
(1387, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:18:09.417521'),
(1388, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:18:11.143251'),
(1389, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:30:56.364737'),
(1390, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:30:58.058180'),
(1391, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:36:59.909311'),
(1392, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:38:02.733476'),
(1393, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:38:57.674218'),
(1394, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:40:08.809297'),
(1395, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:41:07.731686'),
(1396, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:43:22.346772'),
(1397, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:43:24.495994'),
(1398, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:44:14.577920'),
(1399, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:46:13.976750'),
(1400, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:47:45.132474'),
(1401, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:50:24.323973'),
(1402, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:51:13.312447'),
(1403, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:51:17.324654'),
(1404, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:52:07.386520'),
(1405, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:52:57.328211'),
(1406, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:53:36.595901'),
(1407, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-07 07:54:18.543778'),
(1408, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:55:56.615399'),
(1409, 'stock-management-link', '\n                      \n                      Item List\n                    53 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:56:39.688341'),
(1410, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:56:40.254708'),
(1411, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:56:41.950750'),
(1412, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-10-07 07:56:45.074748'),
(1413, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:46:52.151985'),
(1414, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:46:53.900159'),
(1415, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:48:06.087319'),
(1416, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:49:03.256031'),
(1417, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:49:08.581519'),
(1418, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:49:10.244490'),
(1419, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:54:32.929624'),
(1420, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:55:37.070916'),
(1421, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:56:23.353484'),
(1422, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:57:16.499888'),
(1423, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-21 11:57:54.185957'),
(1424, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:19:55.290187'),
(1425, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:20:00.633060'),
(1426, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:28:55.324414'),
(1427, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:29:51.017598'),
(1428, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:30:06.638929'),
(1429, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:31:36.713149'),
(1430, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:32:30.362260'),
(1431, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:34:16.031051'),
(1432, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:35:28.983462'),
(1433, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:35:42.129542'),
(1434, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:36:25.278969'),
(1435, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:38:29.159882'),
(1436, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:39:09.632252'),
(1437, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:40:45.058948'),
(1438, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:43:23.431591'),
(1439, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:43:53.123217'),
(1440, 'stock-management-link', '\n                      \n                      Item List\n                    54 | PANORAMA ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:44:13.014705'),
(1441, 'stock-list-payments-tab', '\n             Payments\n        ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:44:14.574131'),
(1442, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:44:27.640779'),
(1443, 'end-product-tab', '\n            Stocks Request\n        ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:44:58.122203'),
(1444, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-27 09:45:38.705491'),
(1445, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-10-27 11:57:03.834913'),
(1446, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-10-27 11:57:07.406245'),
(1447, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'danson@panoramaengineering.com', '', '2020-10-27 11:59:41.745323'),
(1448, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-10-27 12:00:46.197728'),
(1449, 'reports-link', 'Reports all_profit_loss', 'danson@panoramaengineering.com', '', '2020-10-27 12:01:05.352678'),
(1450, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-10-27 12:02:14.238054'),
(1451, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-10-27 12:02:22.104478'),
(1452, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'danson@panoramaengineering.com', '', '2020-10-27 12:02:48.748387'),
(1453, 'Inventory-management-module', 'Inventory Management Module', 'danson@panoramaengineering.com', '', '2020-10-27 12:05:37.731237'),
(1454, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'danson@panoramaengineering.com', '', '2020-10-27 12:05:45.602050'),
(1455, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-10-27 12:06:48.184224'),
(1456, 'reports-link', 'Reports all_stock_items', 'danson@panoramaengineering.com', '', '2020-10-27 12:07:05.586431'),
(1457, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'danson@panoramaengineering.com', '', '2020-10-27 12:08:51.866711'),
(1458, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:35:03.670154'),
(1459, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:35:06.800486'),
(1460, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:39:05.714972'),
(1461, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:40:23.830573'),
(1462, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:41:42.690030'),
(1463, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:42:42.305144'),
(1464, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:44:08.474673'),
(1465, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:45:03.182721'),
(1466, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:45:07.502215'),
(1467, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-28 08:47:16.613187'),
(1468, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:40:59.196749'),
(1469, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:41:01.949067'),
(1470, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:41:17.086941'),
(1471, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:42:22.129120'),
(1472, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:42:34.613408'),
(1473, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:43:22.771711'),
(1474, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:44:19.082359'),
(1475, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:44:23.682985'),
(1476, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:44:28.761653'),
(1477, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:44:32.456424'),
(1478, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:46:09.272341'),
(1479, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-10-29 06:47:29.156497'),
(1480, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:04:21.359231'),
(1481, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:04:23.549007'),
(1482, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:05:20.511662'),
(1483, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:06:59.667560'),
(1484, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:08:50.934720'),
(1485, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:11:04.426442'),
(1486, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:12:12.939243'),
(1487, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:13:17.893209'),
(1488, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:14:04.507260'),
(1489, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:15:17.018499'),
(1490, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:15:28.303203'),
(1491, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:16:44.579147'),
(1492, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:17:33.615126'),
(1493, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:18:34.765987'),
(1494, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:18:50.813172'),
(1495, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:19:37.133218'),
(1496, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:19:40.546076'),
(1497, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:24:38.488966'),
(1498, 'Inventory-management-module', 'Inventory Management Module', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:42:41.076303'),
(1499, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:42:44.248426'),
(1500, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'mbugua@panoramaengineering.com', '', '2020-11-04 09:43:56.061247'),
(1501, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-11-06 08:53:52.700293'),
(1502, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:53:54.908113'),
(1503, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:54:15.484085'),
(1504, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:54:35.269785'),
(1505, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:54:48.776134'),
(1506, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:55:09.596735'),
(1507, 'supplier-ledger-management-link', '\n                     \n                     Supplier Ledger\n                   ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:55:35.877581'),
(1508, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:55:58.505132'),
(1509, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:15.685523'),
(1510, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:18.507429'),
(1511, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:19.083345'),
(1512, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:21.061432'),
(1513, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:25.074701'),
(1514, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2020-11-06 08:56:59.763109'),
(1515, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2020-11-06 08:57:13.804920'),
(1516, 'admin-logs-link', '\n                  \n                  Logs\n                ', 'inventory@panoramaengineering.com', '', '2020-11-15 14:22:57.344278'),
(1517, 'activity_logs_tab', 'Activity Logs', 'inventory@panoramaengineering.com', '', '2020-11-15 14:23:00.114584'),
(1518, 'sign_in_logs_tab', 'Sign in Logs', 'inventory@panoramaengineering.com', '', '2020-11-15 14:24:01.168676'),
(1519, 'system-info-link', '\n                  \n                  System Information\n                ', 'inventory@panoramaengineering.com', '', '2020-11-15 14:24:11.578893'),
(1520, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-11-24 08:23:34.361757'),
(1521, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-11-24 08:23:39.475341'),
(1522, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:21.592878'),
(1523, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:24.802244'),
(1524, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:37.222749'),
(1525, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:37.807144'),
(1526, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:39.602425'),
(1527, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:41.877274'),
(1528, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:44.624111'),
(1529, 'end-product-management-link', '\n                     \n                     End Product List\n                   12 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:51.297395'),
(1530, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 08:31:51.817767'),
(1531, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-04 10:24:53.958322'),
(1532, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-04 10:24:55.378190'),
(1533, 'stock-management-link', '\n                      \n                      Item List\n                    79 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-04 10:24:59.721892'),
(1534, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 10:25:00.284382'),
(1535, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 10:25:04.613232'),
(1536, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-04 11:28:40.966244'),
(1537, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-04 11:28:43.106889'),
(1538, 'stock-management-link', '\n                      \n                      Item List\n                    80 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-04 11:28:46.298685'),
(1539, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-04 11:28:47.025182'),
(1540, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-05 12:10:45.805691'),
(1541, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-05 12:10:47.218157'),
(1542, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-05 12:11:05.932584'),
(1543, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-05 12:11:06.557893'),
(1544, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-07 05:04:47.908149'),
(1545, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:04:48.975492'),
(1546, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:04:50.969761'),
(1547, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:04:51.539001'),
(1548, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:05.263168'),
(1549, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:09.861937'),
(1550, 'stock-management-link', '\n                      \n                      Item List\n                    79 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:14.095744'),
(1551, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:14.743344'),
(1552, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:16.519094'),
(1553, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 05:05:18.126614'),
(1554, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-07 07:21:48.558109'),
(1555, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 07:21:52.245530'),
(1556, 'stock-management-link', '\n                      \n                      Item List\n                    73 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 07:21:57.317463'),
(1557, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 07:22:00.261114'),
(1558, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 07:22:09.678327'),
(1559, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-07 14:33:06.837723'),
(1560, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:33:07.993777'),
(1561, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:35:34.922304'),
(1562, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:36:47.541114'),
(1563, 'end-product-management-link', '\n                     \n                     End Product List\n                   12 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:36:52.574340'),
(1564, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:36:53.081402'),
(1565, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:36:54.789789'),
(1566, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:36:57.940689'),
(1567, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:22.374758'),
(1568, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:27.910273'),
(1569, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:47.514970'),
(1570, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:50.434753'),
(1571, 'stock-management-link', '\n                      \n                      Item List\n                    82 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:52.418983'),
(1572, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:53.206081'),
(1573, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:37:54.537420'),
(1574, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:16.463332'),
(1575, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:18.690926'),
(1576, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:30.007271'),
(1577, 'stock-management-link', '\n                      \n                      Item List\n                    82 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:32.706810'),
(1578, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:33.151995'),
(1579, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:34.731225'),
(1580, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:38:39.061364'),
(1581, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:23.249590'),
(1582, 'end-product-management-link', '\n                     \n                     End Product List\n                   14 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:28.524669'),
(1583, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:29.200348'),
(1584, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:30.289295'),
(1585, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:41.392329'),
(1586, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:39:53.737428'),
(1587, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-07 14:40:06.604516'),
(1588, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-07 14:40:08.526823'),
(1589, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-07 15:00:40.432238'),
(1590, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Test                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2020-12-07 07:35:32\n                      \n                    \n                  ', 'inventory@panoramaengineering.com', '', '2020-12-07 15:00:40.991198'),
(1591, 'pending_approval_tab', ' New Stocks Pending Approval', 'inventory@panoramaengineering.com', '', '2020-12-07 15:00:41.612432'),
(1592, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-09 07:51:46.115850'),
(1593, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:51:53.122297'),
(1594, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:52:00.675378'),
(1595, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:52:01.223685'),
(1596, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:52:04.283169'),
(1597, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:52:08.397254'),
(1598, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:54:24.163410'),
(1599, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:54:26.374287'),
(1600, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-09 07:54:26.879727'),
(1601, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-09 08:40:29.153779'),
(1602, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-09 08:40:33.628132'),
(1603, 'stock-management-link', '\n                      \n                      Item List\n                    80 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-09 08:40:35.313674'),
(1604, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-09 08:40:38.042030'),
(1605, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-11 13:53:52.338392'),
(1606, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-11 13:53:54.499437'),
(1607, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-11 13:53:59.169233'),
(1608, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:21.077748'),
(1609, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:22.960784'),
(1610, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:29.706684'),
(1611, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:33.281761'),
(1612, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:33.962666'),
(1613, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:35.083044'),
(1614, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2020-12-11 21:54:58.247309'),
(1615, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-02-08 07:42:26.917723'),
(1616, 'reports-link', 'Reports all_stock_items', 'inventory@panoramaengineering.com', '', '2021-02-08 07:42:30.518882'),
(1617, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-03-31 17:37:37.710293'),
(1618, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:37:39.208848'),
(1619, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:41:08.617755'),
(1620, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:41:33.244312'),
(1621, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:41:33.669720'),
(1622, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:41:33.825302'),
(1623, 'open-dashboard-project-payments-modal', 'Dashboard Total Profits ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:42:08.508265'),
(1624, 'open-dashboard-project-payments-modal', 'Dashboard Stocks Payments', 'inventory@panoramaengineering.com', '', '2021-03-31 17:42:38.003186'),
(1625, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2021-03-31 17:44:21.346329'),
(1626, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-04-06 11:55:43.461334'),
(1627, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-04-06 11:55:45.588438'),
(1628, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-04-14 10:17:38.214408'),
(1629, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-04-14 10:17:39.688248');
INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(1630, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2021-04-14 10:18:58.213886'),
(1631, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2021-04-14 10:19:21.321522'),
(1632, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2021-04-14 10:19:21.975402'),
(1633, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-07-02 05:58:27.815887'),
(1634, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-07-02 05:58:28.669564'),
(1635, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-07-15 09:12:36.905335'),
(1636, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-07-15 09:12:37.739981'),
(1637, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-22 06:07:20.428909'),
(1638, 'reports-link', 'Reports all_stock_items', 'inventory@panoramaengineering.com', '', '2021-07-22 06:07:26.420944'),
(1639, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-22 06:07:29.672952'),
(1640, 'reports-link', 'Reports all_paid_invoices', 'inventory@panoramaengineering.com', '', '2021-07-22 06:07:32.620394'),
(1641, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-22 06:07:35.570903'),
(1642, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-25 16:52:02.391158'),
(1643, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-07-26 09:55:27.954731'),
(1644, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-07-26 09:55:29.859793'),
(1645, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-26 09:55:30.201668'),
(1646, 'reports-link', 'Reports all_profit_loss', 'inventory@panoramaengineering.com', '', '2021-07-26 09:55:33.800354'),
(1647, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2021-07-26 09:56:06.987762'),
(1648, 'reports-link', 'Reports stocks_in_production', 'inventory@panoramaengineering.com', '', '2021-07-26 09:56:14.714985'),
(1649, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:35:29.447757'),
(1650, 'admin-user-management-link', '\n                  \n                  User Management\n                ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:35:34.626553'),
(1651, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:02.148218'),
(1652, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:03.404020'),
(1653, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:06.754177'),
(1654, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:51.375285'),
(1655, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:51.901844'),
(1656, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2021-08-16 10:37:54.310033'),
(1657, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:10.891359'),
(1658, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:12.022174'),
(1659, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:18.710369'),
(1660, 'end-product-management-link', '\n                     \n                     End Product List\n                   14 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:27.577941'),
(1661, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:28.027007'),
(1662, 'product-delivery-tab', '\n            Product Deliveries\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:30.978087'),
(1663, 'return-inwards-tab', '\n            Return Inwards\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:38.893800'),
(1664, 'stocks-used-tab', '\n            Stocks Used\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:46.244728'),
(1665, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:21:53.722187'),
(1666, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:24:00.465032'),
(1667, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:05.415602'),
(1668, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:08.653297'),
(1669, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:10.092121'),
(1670, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:10.592227'),
(1671, 'end-product-tab', '\n            Stocks Request\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:12.572654'),
(1672, 'stocks-returns-tab', '\n            Stocks Returns\n        ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:13.715045'),
(1673, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:16.763389'),
(1674, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2021-08-17 05:25:26.720387'),
(1675, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-08-24 04:51:53.726808'),
(1676, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2021-09-04 21:26:52.749054'),
(1677, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2021-09-04 21:26:53.414403'),
(1678, 'stock-management-link', '\n                      \n                      Item List\n                    81 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2021-09-04 21:26:55.622631'),
(1679, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2021-09-04 21:26:56.143923'),
(1680, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2021-09-04 21:26:57.374892'),
(1681, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2021-09-04 21:27:19.529869'),
(1682, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2021-09-06 07:57:42.937065'),
(1683, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-01-25 06:57:27.674465'),
(1684, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-01-25 06:57:28.641218'),
(1685, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2022-01-25 06:58:39.734623'),
(1686, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2022-01-25 06:59:55.826675'),
(1687, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 05:24:48.298871'),
(1688, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 05:24:49.607576'),
(1689, 'user-profile-link', 'User Profile', 'inventory@panoramaengineering.com', '', '2022-02-14 05:44:36.657264'),
(1690, 'admin-user-management-link', '\n                  \n                  User Management\n                ', 'inventory@panoramaengineering.com', '', '2022-02-14 05:44:51.317301'),
(1691, 'admin-user-management-link', '\n                     \n                     Resource Sheet\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 05:46:05.910160'),
(1692, 'admin-user-management-link', '\n                  \n                  User Management\n                ', 'inventory@panoramaengineering.com', '', '2022-02-14 05:46:06.242359'),
(1693, 'Inventory-management-module', 'Inventory Management Module', 'justus@panoramaengineering.com', '', '2022-02-14 05:47:40.661221'),
(1694, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'justus@panoramaengineering.com', '', '2022-02-14 05:47:41.924021'),
(1695, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 05:58:06.001606'),
(1696, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 05:58:08.083437'),
(1697, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:33:58.799003'),
(1698, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:34:00.065593'),
(1699, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:34:07.131601'),
(1700, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:35:45.953693'),
(1701, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:35:50.283582'),
(1702, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:35:54.589350'),
(1703, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:36:24.780703'),
(1704, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:48:14.295517'),
(1705, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:48:14.982885'),
(1706, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:48:40.850876'),
(1707, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:48:41.648292'),
(1708, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:48:48.590704'),
(1709, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:50:05.777072'),
(1710, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:50:06.588317'),
(1711, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:50:11.253487'),
(1712, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:51:26.786080'),
(1713, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:51:30.959814'),
(1714, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 09:52:07.908869'),
(1715, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:52:08.546968'),
(1716, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:52:16.807665'),
(1717, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:52:21.555398'),
(1718, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:52:23.642565'),
(1719, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 09:53:28.828179'),
(1720, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 12:40:20.391614'),
(1721, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:40:22.581888'),
(1722, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:41:29.355765'),
(1723, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 12:42:40.174220'),
(1724, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:42:42.323265'),
(1725, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:41.389013'),
(1726, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:47.450068'),
(1727, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Test Stock                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2022-02-14 05:43:38\n                      \n                    \n                  ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:48.098487'),
(1728, 'pending_approval_tab', ' New Stocks Pending Approval', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:48.590539'),
(1729, 'approved_stocks_tab', 'Approved Stocks', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:52.621526'),
(1730, 'pending_approval_tab', 'please reload page New Stocks Pending Approval', 'inventory@panoramaengineering.com', '', '2022-02-14 12:43:54.359460'),
(1731, 'superuser-dashboard-link', '\n                   \n                   \n                     Dashboard\n                   \n                 ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:44:05.836541'),
(1732, 'open-total-projects-modal', 'Dashboard Total Stocks', 'inventory@panoramaengineering.com', '', '2022-02-14 12:44:11.036987'),
(1733, 'invoice-received-management-link', '\n                  \n                  Invoice Received\n                ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:44:25.145470'),
(1734, 'supplier-management-link', '\n                     \n                     Supplier List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-14 12:44:38.016531'),
(1735, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-15 10:42:36.983937'),
(1736, 'stock-approvals-link', '\n                    \n                                                                \n                      \n                        \n                          \n\n                            Test Stock                          \n                          \n                        \n                                    PETER CHEGE KARIUKI\n                                     2022-02-14 05:43:38\n                      \n                    \n                  ', 'inventory@panoramaengineering.com', '', '2022-02-15 10:42:37.813903'),
(1737, 'pending_approval_tab', ' New Stocks Pending Approval', 'inventory@panoramaengineering.com', '', '2022-02-15 10:42:38.408849'),
(1738, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-15 11:45:14.343739'),
(1739, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-15 11:45:50.876964'),
(1740, 'Inventory-management-module', 'Inventory Management Module', 'justus@panoramaengineering.com', '', '2022-02-18 07:03:19.897977'),
(1741, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'justus@panoramaengineering.com', '', '2022-02-18 07:03:22.757774'),
(1742, 'Inventory-management-module', 'Inventory Management Module', 'justus@panoramaengineering.com', '', '2022-02-18 07:07:53.941770'),
(1743, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'justus@panoramaengineering.com', '', '2022-02-18 07:07:54.973178'),
(1744, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-19 18:03:54.395166'),
(1745, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-19 18:04:20.092900'),
(1746, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:40.446599'),
(1747, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:41.492067'),
(1748, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:49.080632'),
(1749, 'reports-link', 'Reports all_stock_items', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:53.139909'),
(1750, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:56.436396'),
(1751, 'reports-link', 'Reports all_end_product', 'inventory@panoramaengineering.com', '', '2022-02-20 11:59:58.523941'),
(1752, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-20 16:40:10.985317'),
(1753, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-20 16:40:21.268160'),
(1754, 'end-product-management-link', '\n                     \n                     End Product List\n                   ', 'inventory@panoramaengineering.com', '', '2022-02-20 16:40:36.095093'),
(1755, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-20 17:10:06.446886'),
(1756, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-20 17:10:10.356378'),
(1757, 'customer-management-link', '\n                      \n                      Customer List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-20 17:10:19.275542'),
(1758, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-20 17:10:47.143167'),
(1759, 'reports-link', '\n              \n              \n                Reports\n              \n            ', 'inventory@panoramaengineering.com', '', '2022-02-21 06:21:46.733698'),
(1760, 'reports-link', 'Reports all_stock_items', 'inventory@panoramaengineering.com', '', '2022-02-21 06:21:51.215181'),
(1761, 'Inventory-management-module', 'Inventory Management Module', 'inventory@panoramaengineering.com', '', '2022-02-21 14:43:33.965931'),
(1762, 'stock-management-link', '\n                    \n                        \n                          INVENTORY MANAGEMENT  \n                        \n                        \n                          MANAGE STOCKS\n                        \n                      \n                   ', 'inventory@panoramaengineering.com', '', '2022-02-21 14:43:35.355063'),
(1763, 'stock-management-link', '\n                      \n                      Item List\n                    ', 'inventory@panoramaengineering.com', '', '2022-02-21 14:43:57.251406'),
(1764, 'stock-management-link', '\n                      \n                      Item List\n                    82 | PANORAMA ', 'inventory@panoramaengineering.com', '', '2022-02-21 14:44:10.442386'),
(1765, 'stock-list-payments-tab', '\n             Payments\n        ', 'inventory@panoramaengineering.com', '', '2022-02-21 14:44:11.064634'),
(1766, 'evidence-doc-tab', '\n            Evidence Documents\n        ', 'inventory@panoramaengineering.com', '', '2022-02-21 14:44:14.560357');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `time_recorded`) VALUES
(8, 'pitarcheizin@gmail.com', '030cd1ed879d9534c2b8e6b2ee87c41bef76e5ae3abe711b5633b31e9ee8879e881dddafdbb6d9ed431e8c4fced611deb117', '2021-03-01 06:39:15.000000'),
(9, 'pitarcheizin@gmail.com', '  b64e018cb48579a21f35fd6164796ece7c6b7e5b23fc1395c632d4b625d716e2044b4f063600b47b3b510a09461edaa8663d', '2021-03-17 06:03:42.239648'),
(10, 'pitarcheizin@gmail.com', '45b669fd7e986636741545db7372efd6936695864410132762734ee88a70432b20bbff455a08816730f37113b02445eb5788', '2021-06-27 12:29:07.202405'),
(11, 'pitarcheizin@gmail.com', '04fcec69a9aaeee615a3b23356bc9f43e1bf85fef2d5ecb14a7b3b7ddcc7ac3e54c1d761d82e7826327b52a5974a8192c777', '2021-06-27 12:36:31.165909'),
(12, 'Pkariuki@cma.or.ke', '5cd54fc64ab2563f7d91e33f3102326d269e2aecc7fe96ea5a1b81d869461c33115620763240b56ec598620c15f9196ad5de', '2021-07-03 14:55:54.554612'),
(13, 'Pkariuki@cma.or.ke', '94c162c3edd6cad65ed6d2215433f9b41774f0b17a9d8da0cd519702d202ac812fa8b0bfe34ddb8bcdde056ecdba1d34a767', '2021-07-21 10:47:21.143900'),
(14, 'pitarcheizin@gmail.com', '888a69720adc89d88815e8c4b73382bcbdeb899f3e3d910dc5a76a5b89a5967d7ea7c022ebb6a0b50ce70d87fe36322fb94f', '2021-07-21 10:50:38.613353'),
(15, 'pitarcheizin@gmail.com', 'abd94b44149de2464e80b5f8250fe549e2735da662e4e54d071af8ebfdddad01451dfc06224a2ad10db712516aa3e05b95f0', '2021-07-21 10:56:13.713175'),
(16, 'pitarcheizin@gmail.com', '8fe0b484d6ef571493dd8f06cf38bac9ca10393e4123fcf61e54fe79b8f92cd806a1b9033cde80371af166ead30a41330374', '2021-07-21 10:59:40.780219'),
(17, 'pitarcheizin@gmail.com', '4a0ebd49ff04833b40ee39d70b91667d4182717508a827820ab537b7cf634f04e8d7c5fc4518f4760c261b8e0a7663ab1e48', '2021-07-21 11:01:33.666833'),
(18, 'pitarcheizin@gmail.com', '4a0ebd49ff04833b40ee39d70b91667d4182717508a827820ab537b7cf634f04e8d7c5fc4518f4760c261b8e0a7663ab1e48', '2021-07-21 11:02:03.777678'),
(19, 'Pkariuki@cma.or.ke', '89871f31941e58e47e5907b5cae39b41e6c7d94ab77d633d11295eea124474f1fd47816b10693a596971ebb16e954a467526', '2021-07-21 11:03:43.560742'),
(20, 'info@potentialstaffing.com', 'beb8683e90ab79c632d2f8d46c363b56f0174ca543875e8a1155170efb4c1595a821151e17f8ea8659a8e8e74836ef147961', '2021-07-31 15:38:37.546936'),
(21, 'pitarcheizin@gmail.com', 'b512f4d82069bd2ad6f81f1a19c8419e7d08d9acfcaa5449c9f22d1b5d9da8b5bbef6b95a7b1a826047c792bfe6f8c94f61f', '2022-02-15 08:49:52.413847'),
(22, 'inventory@panoramaengineering.com', 'f402eea3388c487452e69d696ace5efba16f9019e07b535070656f38fbe36ec6ce7c3488e2ef909b130b8c366b57bca4015a', '2022-02-15 12:33:54.819633'),
(23, 'inventory@panoramaengineering.com', '96117a40dd11efdf51dd66c14d2bf9d5b49f2959d1edf028cbce66314861594d891217297aca32a25b62bee8173305ed2a6e', '2022-02-15 12:41:50.144382'),
(24, 'inventory@panoramaengineering.com', 'd5ac3d40061168f6b8f78e1cb764e146bc7ea2b3d0cfe1a87a32fad2eb5daf0d78e0860de070ed193860fa5c212cfe0b1fdf', '2022-02-15 13:10:55.052536'),
(25, 'inventory@panoramaengineering.com', 'b1a255dcb24189fb5ad559ac2063e8445140a8a60992d6c01dea6bd56304e77d15182d54e63f7a71080124b8d012f60407cc', '2022-02-15 13:13:53.099741'),
(26, 'inventory@panoramaengineering.com', '110edf8c6916145233a84c2d675a23b0c5506917b231f307e00243954bb60d81bbdaa443e7d69df4d9f2c00e10a63b635eeb', '2022-02-15 13:20:24.830098'),
(27, 'inventory@panoramaengineering.com', 'f4989f1b0d30aa23db3d40485cfff0b850335a00f0925d9552ce41ffadc00b361fafc2ddc065873e8c79a06ebe3a36018539', '2022-02-21 16:42:03.541753');

-- --------------------------------------------------------

--
-- Table structure for table `payment_gateway`
--

CREATE TABLE `payment_gateway` (
  `id` int(6) NOT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_gateway`
--

INSERT INTO `payment_gateway` (`id`, `payment_type`, `time_recorded`, `recorded_by`) VALUES
(1, 'Mpesa', '2020-05-07 17:07:49', 'PETER KARIUKI'),
(2, 'Bank', '2020-05-07 17:07:49', 'PETER KARIUKI'),
(3, 'Cash', '2020-05-07 17:08:30', 'PETER KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(6) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `category_name`, `time_recorded`, `recorded_by`) VALUES
(1, 'Furniture', '2020-06-19 11:52:04', 'PETER CHEGE KARIUKI'),
(2, 'Engineering', '2020-06-19 11:52:32', 'PETER CHEGE KARIUKI'),
(3, 'Powder Coating', '2020-06-19 11:54:22', 'PETER CHEGE KARIUKI'),
(4, 'consumables', '2020-07-09 11:34:49', 'DANSON KARIUKI'),
(5, 'Repairs and Maintenance of machinery', '2020-07-09 11:35:31', 'DANSON KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `product_updates`
--

CREATE TABLE `product_updates` (
  `id` int(6) NOT NULL,
  `task_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_recorded` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_updates`
--

INSERT INTO `product_updates` (`id`, `task_id`, `status`, `color_code`, `comments`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(1, 'TASK1', 'In Progress Behind Schedule', 'four', 'We have started this task', '12-Mar-2020', '2020-03-12 08:14:16.866722', 'MARO JILLO ABDALLA'),
(2, 'TASK2', 'In Progress Within Schedule', 'three', 'Add Comments', '12-Mar-2020', '2020-03-12 08:23:44.654475', 'MARO JILLO ABDALLA'),
(3, 'TASK2', 'Continous', 'one', 'Add Comments', '12-Mar-2020', '2020-03-12 08:27:24.233335', 'MARO JILLO ABDALLA'),
(7, 'TASK2', 'Not Started', 'five', '', '12-Mar-2020', '2020-03-12 09:03:48.970476', 'PETER CHEGE KARIUKI'),
(8, 'TASK4', 'In Progress Within Schedule', 'three', 'fsdfdsf', '12-Mar-2020', '2020-03-12 09:13:27.230733', 'PETER CHEGE KARIUKI'),
(11, 'TASK2', 'Continous', 'one', '', '12-Mar-2020', '2020-03-12 11:37:57.439451', 'PETER CHEGE KARIUKI'),
(12, 'TASK7', 'In Progress Within Schedule', 'three', '', '12-Mar-2020', '2020-03-12 11:51:05.455149', 'PETER CHEGE KARIUKI'),
(13, 'TASK7', 'Completed', 'two', '', '12-Mar-2020', '2020-03-12 11:51:19.163843', 'PETER CHEGE KARIUKI'),
(17, 'TASK10', 'In Progress Within Schedule', 'three', '', '16-Mar-2020', '2020-03-16 06:26:16.714730', 'MARO JILLO ABDALLA'),
(18, 'TASK4', 'Continous', 'one', '', '24-Apr-2020', '2020-04-24 10:13:55.339557', 'PETER CHEGE KARIUKI'),
(19, 'TASK14', 'In Progress Within Schedule', 'three', '', '27-Apr-2020', '2020-04-27 15:39:24.866378', 'PETER CHEGE KARIUKI'),
(20, 'TASK15', 'Continous', 'one', '', '27-Apr-2020', '2020-04-27 16:02:20.345000', 'PETER CHEGE KARIUKI'),
(21, 'TASK10', 'Continous', 'one', '', '09-May-2020', '2020-05-09 15:31:08.180126', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `sign_in_logs`
--

CREATE TABLE `sign_in_logs` (
  `id` int(6) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `user_type` varchar(50) NOT NULL DEFAULT 'default',
  `time_signed_in` varchar(50) DEFAULT NULL,
  `time_signed_out` varchar(50) DEFAULT NULL,
  `date_recorded` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sign_in_logs`
--

INSERT INTO `sign_in_logs` (`id`, `email`, `name`, `ip_address`, `user_type`, `time_signed_in`, `time_signed_out`, `date_recorded`) VALUES
(56, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/07/15 13:27:41', '2020/07/15 14:13:21', '2020/07/15'),
(57, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/07/15 14:24:12', '2020/07/15 15:14:02', '2020/07/15'),
(58, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/07/15 15:22:17', '2020/07/15 15:25:18', '2020/07/15'),
(59, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/07/15 15:25:23', '2020/07/15 15:38:03', '2020/07/15'),
(60, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/07/15 15:38:57', '2020/07/15 15:39:54', '2020/07/15'),
(61, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/07/15 15:40:07', NULL, '2020/07/15'),
(62, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.230', 'default', '2020/07/16 10:21:53', NULL, '2020/07/16'),
(63, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.230', 'default', '2020/07/16 20:37:01', '2020/07/16 22:19:08', '2020/07/16'),
(64, 'osino@panoramaengineering.com', 'RUTH OSINO', '197.254.121.226', 'default', '2020/07/17 15:42:37', NULL, '2020/07/17'),
(65, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.230', 'default', '2020/07/17 19:23:02', NULL, '2020/07/17'),
(66, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.230', 'default', '2020/07/17 19:56:37', NULL, '2020/07/17'),
(67, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.141', 'default', '2020/07/19 17:58:38', NULL, '2020/07/19'),
(68, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.141', 'default', '2020/07/20 13:31:05', NULL, '2020/07/20'),
(69, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.98.181', 'default', '2020/07/22 12:15:47', '2020/07/22 13:32:04', '2020/07/22'),
(70, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.98.181', 'default', '2020/07/22 13:32:12', '2020/07/22 13:47:16', '2020/07/22'),
(71, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.98.181', 'default', '2020/07/22 13:49:54', NULL, '2020/07/22'),
(72, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.72.209.53', 'default', '2020/07/23 14:22:09', '2020/07/23 15:00:08', '2020/07/23'),
(73, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.80.96.9', 'default', '2020/08/03 14:52:56', NULL, '2020/08/03'),
(74, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/08/04 09:05:53', '2020/08/04 09:06:07', '2020/08/04'),
(75, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/08/04 15:26:26', NULL, '2020/08/04'),
(76, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/08/06 16:10:17', NULL, '2020/08/06'),
(77, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '197.254.121.226', 'default', '2020/08/10 11:22:07', '2020/08/10 14:36:44', '2020/08/10'),
(78, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '196.201.218.76', 'default', '2020/08/12 12:38:12', NULL, '2020/08/12'),
(79, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/08/12 15:10:55', '2020/08/12 15:11:21', '2020/08/12'),
(80, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/12 15:12:32', '2020/08/12 16:01:38', '2020/08/12'),
(81, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/13 12:11:06', NULL, '2020/08/13'),
(82, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/13 12:21:37', NULL, '2020/08/13'),
(83, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/14 09:43:40', '2020/08/14 11:45:39', '2020/08/14'),
(84, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/08/14 10:00:10', NULL, '2020/08/14'),
(85, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/14 14:09:08', '2020/08/14 15:25:54', '2020/08/14'),
(86, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/14 15:29:44', '2020/08/14 15:53:50', '2020/08/14'),
(87, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/15 09:06:24', '2020/08/15 11:06:24', '2020/08/15'),
(88, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/15 11:06:42', '2020/08/15 12:16:21', '2020/08/15'),
(89, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/15 12:16:50', NULL, '2020/08/15'),
(90, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/17 09:32:57', '2020/08/17 13:00:10', '2020/08/17'),
(91, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.90.7.80', 'default', '2020/08/18 11:38:30', NULL, '2020/08/18'),
(92, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/18 14:02:25', NULL, '2020/08/18'),
(93, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/18 14:29:08', NULL, '2020/08/18'),
(94, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/18 16:33:19', NULL, '2020/08/18'),
(95, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/20 10:42:22', NULL, '2020/08/20'),
(96, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/20 13:42:32', NULL, '2020/08/20'),
(97, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/21 09:36:45', '2020/08/21 14:23:21', '2020/08/21'),
(98, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/24 15:32:58', '2020/08/24 16:00:12', '2020/08/24'),
(99, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/08/24 15:43:57', NULL, '2020/08/24'),
(100, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/25 08:58:43', '2020/08/25 12:21:16', '2020/08/25'),
(101, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/25 12:21:47', '2020/08/25 14:22:39', '2020/08/25'),
(102, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/25 13:12:41', NULL, '2020/08/25'),
(103, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/26 09:59:37', NULL, '2020/08/26'),
(104, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/26 14:41:45', NULL, '2020/08/26'),
(105, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/08/28 11:25:36', NULL, '2020/08/28'),
(106, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/28 11:31:07', NULL, '2020/08/28'),
(107, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/08/28 13:40:44', '2020/08/28 16:00:26', '2020/08/28'),
(108, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/09/02 09:41:53', '2020/09/02 12:03:14', '2020/09/02'),
(109, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/09/02 14:42:50', '2020/09/02 15:57:55', '2020/09/02'),
(110, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/09/03 15:20:26', NULL, '2020/09/03'),
(111, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '197.254.121.226', 'default', '2020/09/04 15:41:34', NULL, '2020/09/04'),
(112, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/09/08 15:09:10', NULL, '2020/09/08'),
(113, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/09/08 17:56:07', NULL, '2020/09/08'),
(114, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '197.254.121.226', 'default', '2020/09/11 10:02:53', NULL, '2020/09/11'),
(115, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '62.24.124.215', 'default', '2020/09/16 15:34:46', NULL, '2020/09/16'),
(116, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '62.24.124.215', 'default', '2020/09/17 09:20:11', '2020/09/17 16:04:33', '2020/09/17'),
(117, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '62.24.124.215', 'default', '2020/09/17 10:42:24', NULL, '2020/09/17'),
(118, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/09/18 11:07:58', '2020/09/18 13:58:51', '2020/09/18'),
(119, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/09/18 14:43:59', '2020/09/18 16:02:09', '2020/09/18'),
(120, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '102.68.77.126', 'default', '2020/10/01 16:22:44', '2020/10/01 16:26:11', '2020/10/01'),
(121, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '102.68.77.126', 'default', '2020/10/01 16:29:05', NULL, '2020/10/01'),
(122, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/10/07 09:30:45', NULL, '2020/10/07'),
(123, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/10/07 10:29:53', '2020/10/07 11:53:48', '2020/10/07'),
(124, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/10/09 09:25:16', NULL, '2020/10/09'),
(125, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/10/21 14:46:33', '2020/10/21 15:57:04', '2020/10/21'),
(126, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/10/27 12:19:46', '2020/10/27 14:54:52', '2020/10/27'),
(127, 'danson@panoramaengineering.com', 'DANSON KARIUKI', '102.68.77.126', 'default', '2020/10/27 14:30:22', NULL, '2020/10/27'),
(128, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/10/28 11:27:57', '2020/10/28 15:29:23', '2020/10/28'),
(129, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/10/29 09:40:31', NULL, '2020/10/29'),
(130, 'mbugua@panoramaengineering.com', 'BENCO MBUGUA', '102.68.77.126', 'default', '2020/11/04 12:04:03', '2020/11/04 15:25:32', '2020/11/04'),
(131, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/11/06 11:53:45', NULL, '2020/11/06'),
(132, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/11/15 17:22:40', NULL, '2020/11/15'),
(133, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/11/24 11:23:26', NULL, '2020/11/24'),
(134, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/04 11:31:17', NULL, '2020/12/04'),
(135, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/04 13:24:34', NULL, '2020/12/04'),
(136, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/04 14:28:36', NULL, '2020/12/04'),
(137, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/05 15:10:40', NULL, '2020/12/05'),
(138, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/07 08:04:41', NULL, '2020/12/07'),
(139, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/07 10:21:45', NULL, '2020/12/07'),
(140, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/07 17:32:54', NULL, '2020/12/07'),
(141, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/09 10:51:08', NULL, '2020/12/09'),
(142, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/11 16:53:47', NULL, '2020/12/11'),
(143, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/12 00:54:07', NULL, '2020/12/12'),
(144, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/18 09:29:36', NULL, '2020/12/18'),
(145, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2020/12/22 10:48:07', NULL, '2020/12/22'),
(146, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2021/02/08 10:42:10', NULL, '2021/02/08'),
(147, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.68.77.126', 'default', '2021/02/12 15:26:42', NULL, '2021/02/12'),
(148, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/02/25 20:48:23', NULL, '2021/02/25'),
(149, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/03/31 20:37:16', NULL, '2021/03/31'),
(150, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/04/06 14:55:39', NULL, '2021/04/06'),
(151, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/04/14 13:16:57', NULL, '2021/04/14'),
(152, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '102.167.201.214', 'default', '2021/07/02 08:58:17', NULL, '2021/07/02'),
(153, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2021/07/15 12:12:27', NULL, '2021/07/15'),
(154, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/07/22 09:00:47', NULL, '2021/07/22'),
(155, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/07/25 19:51:52', NULL, '2021/07/25'),
(156, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/07/26 12:55:09', NULL, '2021/07/26'),
(157, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/08/16 13:35:20', NULL, '2021/08/16'),
(158, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/08/17 08:20:45', NULL, '2021/08/17'),
(159, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2021/08/24 07:51:44', NULL, '2021/08/24'),
(160, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '105.63.166.230', 'default', '2021/09/05 00:26:48', NULL, '2021/09/05'),
(161, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.139.160.21', 'default', '2021/09/06 10:57:37', NULL, '2021/09/06'),
(162, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/01/25 09:57:05', NULL, '2022/01/25'),
(163, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 08:24:08', '2022/02/14 08:46:28', '2022/02/14'),
(164, 'justus@panoramaengineering.com', 'JUSTUS M', '41.203.222.101', 'default', '2022/02/14 08:47:35', '2022/02/14 08:49:23', '2022/02/14'),
(165, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 08:57:15', '2022/02/14 09:02:45', '2022/02/14'),
(166, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 08:57:50', NULL, '2022/02/14'),
(167, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 12:22:56', '2022/02/14 12:23:24', '2022/02/14'),
(168, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 12:33:37', NULL, '2022/02/14'),
(169, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 15:40:11', '2022/02/14 15:41:32', '2022/02/14'),
(170, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/14 15:41:57', NULL, '2022/02/14'),
(171, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '196.201.210.147', 'default', '2022/02/15 13:41:51', '2022/02/15 13:42:43', '2022/02/15'),
(172, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '105.161.74.75', 'default', '2022/02/15 13:44:18', NULL, '2022/02/15'),
(173, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:09:40', '2022/02/15 14:11:57', '2022/02/15'),
(174, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:13:00', '2022/02/15 14:17:27', '2022/02/15'),
(175, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:17:44', '2022/02/15 14:17:51', '2022/02/15'),
(176, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:18:11', '2022/02/15 14:18:15', '2022/02/15'),
(177, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:23:15', '2022/02/15 14:23:19', '2022/02/15'),
(178, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:43:23', '2022/02/15 14:46:12', '2022/02/15'),
(179, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 14:58:09', '2022/02/15 14:58:29', '2022/02/15'),
(180, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:17:21', '2022/02/15 15:17:38', '2022/02/15'),
(181, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:18:58', '2022/02/15 15:19:28', '2022/02/15'),
(182, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:19:35', '2022/02/15 15:20:01', '2022/02/15'),
(183, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:20:22', '2022/02/15 15:21:18', '2022/02/15'),
(184, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:21:26', '2022/02/15 15:21:34', '2022/02/15'),
(185, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:32:26', '2022/02/15 15:32:41', '2022/02/15'),
(186, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:33:24', '2022/02/15 15:33:31', '2022/02/15'),
(187, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:41:14', '2022/02/15 15:41:21', '2022/02/15'),
(188, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '41.203.222.101', 'default', '2022/02/15 15:58:07', '2022/02/15 15:58:14', '2022/02/15'),
(189, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/15 22:39:32', '2022/02/15 22:39:49', '2022/02/15'),
(190, 'justus@panoramaengineering.com', 'JUSTUS M', '62.24.124.150', 'default', '2022/02/18 10:03:05', '2022/02/18 10:03:26', '2022/02/18'),
(191, 'justus@panoramaengineering.com', 'JUSTUS M', '62.24.124.150', 'default', '2022/02/18 10:03:52', '2022/02/18 10:04:04', '2022/02/18'),
(192, 'justus@panoramaengineering.com', 'JUSTUS M', '62.24.124.150', 'default', '2022/02/18 10:05:11', NULL, '2022/02/18'),
(193, 'justus@panoramaengineering.com', 'JUSTUS M', '62.24.124.150', 'default', '2022/02/18 10:07:17', NULL, '2022/02/18'),
(194, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/19 21:02:30', NULL, '2022/02/19'),
(195, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/20 14:59:36', NULL, '2022/02/20'),
(196, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/20 19:34:31', '2022/02/20 19:40:37', '2022/02/20'),
(197, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/20 19:40:58', NULL, '2022/02/20'),
(198, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/20 20:05:31', NULL, '2022/02/20'),
(199, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/20 20:08:54', NULL, '2022/02/20'),
(200, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/21 09:21:41', NULL, '2022/02/21'),
(201, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '154.159.246.58', 'default', '2022/02/21 17:42:40', '2022/02/21 19:41:52', '2022/02/21');

-- --------------------------------------------------------

--
-- Table structure for table `single_product`
--

CREATE TABLE `single_product` (
  `id` int(6) NOT NULL,
  `end_product_ref` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `order_total` varchar(100) DEFAULT NULL,
  `stock_remaining` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_recorded` varchar(100) DEFAULT NULL,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `single_product`
--

INSERT INTO `single_product` (`id`, `end_product_ref`, `product_name`, `unit_price`, `qtt`, `total`, `order_total`, `stock_remaining`, `time_recorded`, `date_recorded`, `recorded_by`) VALUES
(10, '8', '7', '3200', '1', '3200.00', NULL, '0', '2020-08-14 12:37:49', '14-Aug-2020', 'BENCO MBUGUA'),
(11, '35', '8', '684', '10', '6840.00', NULL, '0', '2020-09-17 07:00:14', '17-Sep-2020', 'BENCO MBUGUA'),
(12, '30', '8', '250', '11', '2750.00', NULL, '0', '2020-09-17 07:01:07', '17-Sep-2020', 'BENCO MBUGUA'),
(13, '38', '9', '3', '150', '450.00', NULL, '0', '2020-09-17 07:15:32', '17-Sep-2020', 'BENCO MBUGUA'),
(14, '37', '9', '300', '1', '300.00', NULL, '0', '2020-09-17 07:16:13', '17-Sep-2020', 'BENCO MBUGUA'),
(15, '39', '9', '80', '50', '4000.00', NULL, '0', '2020-09-17 07:16:46', '17-Sep-2020', 'BENCO MBUGUA'),
(16, '34', '9', '2385', '12', '28620.00', NULL, '0', '2020-09-17 07:17:16', '17-Sep-2020', 'BENCO MBUGUA'),
(17, '33', '9', '770', '2', '1540.00', NULL, '0', '2020-09-17 07:17:49', '17-Sep-2020', 'BENCO MBUGUA'),
(18, '36', '9', '1450', '14', '20300.00', NULL, '0', '2020-09-17 07:19:23', '17-Sep-2020', 'BENCO MBUGUA'),
(19, '32', '9', '4045', '3', '12135.00', NULL, '0', '2020-09-17 07:19:59', '17-Sep-2020', 'BENCO MBUGUA'),
(20, '31', '9', '13305', '3', '39915.00', NULL, '0', '2020-09-17 07:20:51', '17-Sep-2020', 'BENCO MBUGUA'),
(21, '42', '9', '1600', '2', '3200.00', NULL, '0', '2020-09-17 07:24:39', '17-Sep-2020', 'BENCO MBUGUA'),
(22, '43', '9', '600', '2', '1200.00', NULL, '0', '2020-09-17 07:25:13', '17-Sep-2020', 'BENCO MBUGUA'),
(25, '29', '8', '370', '15', '5550.00', NULL, '0', '2020-09-18 12:20:27', '18-Sep-2020', 'BENCO MBUGUA'),
(26, '54', '12', '16000', '3', '48000.00', NULL, '0', '2020-10-27 09:44:55', '27-Oct-2020', 'BENCO MBUGUA');

-- --------------------------------------------------------

--
-- Table structure for table `staff_users`
--

CREATE TABLE `staff_users` (
  `EmpNo` varchar(100) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `DepartmentCode` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'active',
  `access_level` varchar(50) NOT NULL DEFAULT 'standard',
  `designation` varchar(250) DEFAULT NULL,
  `emp_photo` varchar(100) NOT NULL DEFAULT 'user.jpg',
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_users`
--

INSERT INTO `staff_users` (`EmpNo`, `Name`, `Email`, `DepartmentCode`, `status`, `access_level`, `designation`, `emp_photo`, `password`) VALUES
('7230', 'PETER CHEGE KARIUKI', 'inventory@panoramaengineering.com', 'ICT', 'active', 'admin', 'Senior Developer', 'J7SN23R.jpg', '21232f297a57a5a743894a0e4a801fc3'),
('PAN002', 'DANSON KARIUKI', 'danson@panoramaengineering.com', 'FA', 'active', 'admin', 'Finance & Administration ', 'user.jpg', '9b95fad8beb731ab4573bc8539576da0'),
('PAN004', 'RUTH OSINO', 'osino2@panoramaengineering.com', 'PROC', 'active', 'standard', 'Procurement', 'user.jpg', '4db39add5599f2b39b80c738b61b20ff'),
('PAN006', 'JUSTUS M', 'justus@panoramaengineering.com', 'PROC', 'active', 'standard', 'Procurement', 'user.jpg', 'd9bdb65295fea37949324fc04be283d2'),
('PANOO1', 'MOFFAT MUTHECI', 'moffat1@panoramaengineering.com', 'DO', 'active', 'admin', 'Director', 'avatar.png', 'ef03863527cbb8cc4ccde0079771e64c'),
('PANOO3', 'BENCO MBUGUA', 'mbugua2@panoramaengineering.com', 'FA', 'active', 'standard', 'finance', 'user.jpg', '7e7ac2ac6c72be395fb43a9e52632e5b'),
('PANOO5', 'NELSON MWANGI', 'nelsonkim2@panoramaengineering.com', 'LO', 'active', 'standard', 'Logistics', 'user.jpg', '06443fc85d1e3ab25c6d8a5867edb171');

-- --------------------------------------------------------

--
-- Table structure for table `stocks_returns`
--

CREATE TABLE `stocks_returns` (
  `id` int(6) NOT NULL,
  `end_product_ref` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `unit_price` varchar(100) DEFAULT NULL,
  `qtt` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `order_total` varchar(100) DEFAULT NULL,
  `stock_remaining` varchar(100) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_recorded` varchar(100) DEFAULT NULL,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_approvers`
--

CREATE TABLE `stock_approvers` (
  `id` int(6) NOT NULL,
  `stock_approver` varchar(100) DEFAULT NULL,
  `stock_id` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_approvers`
--

INSERT INTO `stock_approvers` (`id`, `stock_approver`, `stock_id`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(14, 'danson@panoramaengineering.com', '1', '15-Jul-2020', '2020-07-15 10:52:07', 'PETER CHEGE KARIUKI'),
(15, 'danson@panoramaengineering.com', '2', '15-Jul-2020', '2020-07-15 11:00:37', 'PETER CHEGE KARIUKI'),
(16, 'danson@panoramaengineering.com', '3', '15-Jul-2020', '2020-07-15 11:02:38', 'PETER CHEGE KARIUKI'),
(17, 'danson@panoramaengineering.com', '4', '15-Jul-2020', '2020-07-15 11:06:20', 'PETER CHEGE KARIUKI'),
(18, 'danson@panoramaengineering.com', '5', '15-Jul-2020', '2020-07-15 11:07:19', 'PETER CHEGE KARIUKI'),
(19, 'danson@panoramaengineering.com', '6', '15-Jul-2020', '2020-07-15 11:25:47', 'PETER CHEGE KARIUKI'),
(20, 'danson@panoramaengineering.com', '7', '15-Jul-2020', '2020-07-15 11:26:39', 'PETER CHEGE KARIUKI'),
(21, 'moffat1@panoramaengineering.com', '8', '14-Aug-2020', '2020-08-14 12:32:52', 'BENCO MBUGUA'),
(22, 'danson@panoramaengineering.com', '9', '25-Aug-2020', '2020-08-25 09:27:36', 'BENCO MBUGUA'),
(23, 'danson@panoramaengineering.com', '10', '25-Aug-2020', '2020-08-25 09:28:46', 'BENCO MBUGUA'),
(24, 'danson@panoramaengineering.com', '11', '25-Aug-2020', '2020-08-25 09:30:37', 'BENCO MBUGUA'),
(25, 'danson@panoramaengineering.com', '12', '25-Aug-2020', '2020-08-25 09:35:50', 'BENCO MBUGUA'),
(26, 'danson@panoramaengineering.com', '13', '25-Aug-2020', '2020-08-25 09:39:10', 'BENCO MBUGUA'),
(27, 'danson@panoramaengineering.com', '14', '25-Aug-2020', '2020-08-25 09:42:52', 'BENCO MBUGUA'),
(28, 'danson@panoramaengineering.com', '15', '25-Aug-2020', '2020-08-25 09:44:20', 'BENCO MBUGUA'),
(29, 'danson@panoramaengineering.com', '16', '25-Aug-2020', '2020-08-25 09:45:26', 'BENCO MBUGUA'),
(30, 'danson@panoramaengineering.com', '17', '25-Aug-2020', '2020-08-25 09:46:24', 'BENCO MBUGUA'),
(31, 'danson@panoramaengineering.com', '18', '25-Aug-2020', '2020-08-25 09:47:29', 'BENCO MBUGUA'),
(32, 'danson@panoramaengineering.com', '19', '25-Aug-2020', '2020-08-25 09:48:53', 'BENCO MBUGUA'),
(33, 'danson@panoramaengineering.com', '20', '28-Aug-2020', '2020-08-28 08:49:12', 'BENCO MBUGUA'),
(34, 'danson@panoramaengineering.com', '21', '28-Aug-2020', '2020-08-28 08:50:07', 'BENCO MBUGUA'),
(35, 'danson@panoramaengineering.com', '22', '28-Aug-2020', '2020-08-28 08:50:47', 'BENCO MBUGUA'),
(36, 'danson@panoramaengineering.com', '23', '28-Aug-2020', '2020-08-28 08:51:32', 'BENCO MBUGUA'),
(37, 'danson@panoramaengineering.com', '24', '28-Aug-2020', '2020-08-28 08:54:23', 'BENCO MBUGUA'),
(38, 'danson@panoramaengineering.com', '25', '28-Aug-2020', '2020-08-28 08:59:04', 'BENCO MBUGUA'),
(39, 'danson@panoramaengineering.com', '26', '28-Aug-2020', '2020-08-28 11:04:37', 'BENCO MBUGUA'),
(40, 'danson@panoramaengineering.com', '27', '28-Aug-2020', '2020-08-28 11:08:58', 'BENCO MBUGUA'),
(41, 'danson@panoramaengineering.com', '28', '17-Sep-2020', '2020-09-17 06:22:09', 'BENCO MBUGUA'),
(42, 'danson@panoramaengineering.com', '29', '17-Sep-2020', '2020-09-17 06:23:46', 'BENCO MBUGUA'),
(43, 'danson@panoramaengineering.com', '30', '17-Sep-2020', '2020-09-17 06:24:33', 'BENCO MBUGUA'),
(44, 'danson@panoramaengineering.com', '31', '17-Sep-2020', '2020-09-17 06:26:05', 'BENCO MBUGUA'),
(45, 'danson@panoramaengineering.com', '32', '17-Sep-2020', '2020-09-17 06:34:00', 'BENCO MBUGUA'),
(46, 'danson@panoramaengineering.com', '33', '17-Sep-2020', '2020-09-17 06:35:00', 'BENCO MBUGUA'),
(47, 'danson@panoramaengineering.com', '34', '17-Sep-2020', '2020-09-17 06:35:51', 'BENCO MBUGUA'),
(48, 'danson@panoramaengineering.com', '35', '17-Sep-2020', '2020-09-17 06:37:20', 'BENCO MBUGUA'),
(49, 'danson@panoramaengineering.com', '36', '17-Sep-2020', '2020-09-17 07:03:29', 'BENCO MBUGUA'),
(50, 'danson@panoramaengineering.com', '37', '17-Sep-2020', '2020-09-17 07:08:13', 'BENCO MBUGUA'),
(51, 'danson@panoramaengineering.com', '38', '17-Sep-2020', '2020-09-17 07:09:20', 'BENCO MBUGUA'),
(52, 'danson@panoramaengineering.com', '39', '17-Sep-2020', '2020-09-17 07:10:20', 'BENCO MBUGUA'),
(53, 'danson@panoramaengineering.com', '40', '17-Sep-2020', '2020-09-17 07:11:07', 'BENCO MBUGUA'),
(54, 'danson@panoramaengineering.com', '41', '17-Sep-2020', '2020-09-17 07:11:45', 'BENCO MBUGUA'),
(55, 'danson@panoramaengineering.com', '42', '17-Sep-2020', '2020-09-17 07:23:13', 'BENCO MBUGUA'),
(56, 'danson@panoramaengineering.com', '43', '17-Sep-2020', '2020-09-17 07:23:53', 'BENCO MBUGUA'),
(57, 'danson@panoramaengineering.com', '44', '07-Oct-2020', '2020-10-07 07:37:59', 'BENCO MBUGUA'),
(58, 'danson@panoramaengineering.com', '45', '07-Oct-2020', '2020-10-07 07:38:55', 'BENCO MBUGUA'),
(59, 'danson@panoramaengineering.com', '46', '07-Oct-2020', '2020-10-07 07:40:06', 'BENCO MBUGUA'),
(60, 'danson@panoramaengineering.com', '47', '07-Oct-2020', '2020-10-07 07:44:11', 'BENCO MBUGUA'),
(61, 'danson@panoramaengineering.com', '48', '07-Oct-2020', '2020-10-07 07:46:11', 'BENCO MBUGUA'),
(62, 'danson@panoramaengineering.com', '49', '07-Oct-2020', '2020-10-07 07:47:41', 'BENCO MBUGUA'),
(63, 'danson@panoramaengineering.com', '50', '07-Oct-2020', '2020-10-07 07:52:05', 'BENCO MBUGUA'),
(64, 'danson@panoramaengineering.com', '51', '07-Oct-2020', '2020-10-07 07:52:54', 'BENCO MBUGUA'),
(65, 'danson@panoramaengineering.com', '52', '07-Oct-2020', '2020-10-07 07:53:33', 'BENCO MBUGUA'),
(66, 'danson@panoramaengineering.com', '53', '07-Oct-2020', '2020-10-07 07:54:15', 'BENCO MBUGUA'),
(67, 'danson@panoramaengineering.com', '54', '21-Oct-2020', '2020-10-21 11:54:30', 'BENCO MBUGUA'),
(68, 'danson@panoramaengineering.com', '55', '21-Oct-2020', '2020-10-21 11:55:34', 'BENCO MBUGUA'),
(69, 'danson@panoramaengineering.com', '56', '21-Oct-2020', '2020-10-21 11:56:20', 'BENCO MBUGUA'),
(70, 'danson@panoramaengineering.com', '57', '21-Oct-2020', '2020-10-21 11:57:13', 'BENCO MBUGUA'),
(71, 'danson@panoramaengineering.com', '58', '21-Oct-2020', '2020-10-21 11:57:51', 'BENCO MBUGUA'),
(72, 'danson@panoramaengineering.com', '59', '27-Oct-2020', '2020-10-27 09:28:51', 'BENCO MBUGUA'),
(73, 'danson@panoramaengineering.com', '60', '27-Oct-2020', '2020-10-27 09:29:46', 'BENCO MBUGUA'),
(74, 'danson@panoramaengineering.com', '61', '28-Oct-2020', '2020-10-28 08:39:02', 'BENCO MBUGUA'),
(75, 'danson@panoramaengineering.com', '62', '28-Oct-2020', '2020-10-28 08:40:21', 'BENCO MBUGUA'),
(76, 'danson@panoramaengineering.com', '63', '28-Oct-2020', '2020-10-28 08:41:39', 'BENCO MBUGUA'),
(77, 'danson@panoramaengineering.com', '64', '28-Oct-2020', '2020-10-28 08:42:38', 'BENCO MBUGUA'),
(78, 'danson@panoramaengineering.com', '65', '28-Oct-2020', '2020-10-28 08:47:14', 'BENCO MBUGUA'),
(79, 'danson@panoramaengineering.com', '66', '29-Oct-2020', '2020-10-29 06:43:12', 'BENCO MBUGUA'),
(80, 'danson@panoramaengineering.com', '67', '29-Oct-2020', '2020-10-29 06:43:56', 'BENCO MBUGUA'),
(81, 'danson@panoramaengineering.com', '67', '29-Oct-2020', '2020-10-29 06:44:08', 'BENCO MBUGUA'),
(82, 'danson@panoramaengineering.com', '67', '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA'),
(83, 'danson@panoramaengineering.com', '67', '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA'),
(84, 'danson@panoramaengineering.com', '68', '29-Oct-2020', '2020-10-29 06:45:23', 'BENCO MBUGUA'),
(85, 'danson@panoramaengineering.com', '69', '29-Oct-2020', '2020-10-29 06:47:08', 'BENCO MBUGUA'),
(86, 'danson@panoramaengineering.com', '70', '04-Nov-2020', '2020-11-04 09:05:17', 'BENCO MBUGUA'),
(87, 'danson@panoramaengineering.com', '71', '04-Nov-2020', '2020-11-04 09:06:56', 'BENCO MBUGUA'),
(88, 'danson@panoramaengineering.com', '72', '04-Nov-2020', '2020-11-04 09:08:48', 'BENCO MBUGUA'),
(89, 'danson@panoramaengineering.com', '73', '04-Nov-2020', '2020-11-04 09:11:01', 'BENCO MBUGUA'),
(90, 'danson@panoramaengineering.com', '74', '04-Nov-2020', '2020-11-04 09:12:08', 'BENCO MBUGUA'),
(91, 'danson@panoramaengineering.com', '75', '04-Nov-2020', '2020-11-04 09:13:13', 'BENCO MBUGUA'),
(92, 'danson@panoramaengineering.com', '76', '04-Nov-2020', '2020-11-04 09:14:01', 'BENCO MBUGUA'),
(93, 'danson@panoramaengineering.com', '77', '04-Nov-2020', '2020-11-04 09:16:40', 'BENCO MBUGUA'),
(94, 'danson@panoramaengineering.com', '78', '04-Nov-2020', '2020-11-04 09:17:29', 'BENCO MBUGUA'),
(95, 'danson@panoramaengineering.com', '79', '04-Nov-2020', '2020-11-04 09:18:24', 'BENCO MBUGUA'),
(96, 'danson@panoramaengineering.com', '80', '04-Nov-2020', '2020-11-04 09:24:35', 'BENCO MBUGUA'),
(97, 'danson@panoramaengineering.com', '81', '04-Nov-2020', '2020-11-04 09:43:53', 'BENCO MBUGUA'),
(98, 'inventory@panoramaengineering.com', '82', '07-Dec-2020', '2020-12-07 14:35:32', 'PETER CHEGE KARIUKI'),
(99, 'inventory@panoramaengineering.com', '82', '14-Feb-2222', '2022-02-14 12:43:38', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `stock_item`
--

CREATE TABLE `stock_item` (
  `id` int(6) NOT NULL,
  `reference_no` int(11) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_description` varchar(100) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `supplier_id` varchar(100) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'pending_approval',
  `image` varchar(100) DEFAULT NULL,
  `end_product_id` varchar(100) DEFAULT NULL,
  `date_recorded` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_item`
--

INSERT INTO `stock_item` (`id`, `reference_no`, `item_name`, `item_description`, `category_id`, `supplier_id`, `status`, `image`, `end_product_id`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(14, 1, 'Copper lug compress', '120 sqmm', 'Engineering', 'ZHEJIANG YOMIN ELECTRIC LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 10:52:07', 'PETER CHEGE KARIUKI'),
(15, 2, 'Copper lug  compress', '70sqmm', 'Engineering', 'ZHEJIANG YOMIN ELECTRIC LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:00:37', 'PETER CHEGE KARIUKI'),
(16, 3, 'Copper lug compress', '630sqmm', 'Engineering', 'ZHEJIANG YOMIN ELECTRIC LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:02:38', 'PETER CHEGE KARIUKI'),
(17, 4, 'Expulsion Fuse-cut-out', '11Kv', 'Engineering', 'CHUANG XIONG GROUP TECHNOLOGY LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:06:20', 'PETER CHEGE KARIUKI'),
(18, 5, 'Expulsion Fuse-cut-out', '33Kv', 'Engineering', 'CHUANG XIONG GROUP TECHNOLOGY LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:07:19', 'PETER CHEGE KARIUKI'),
(19, 6, 'Termination Kits', '11Kv', 'Engineering', 'CHUANG XIONG GROUP TECHNOLOGY LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:25:47', 'PETER CHEGE KARIUKI'),
(20, 7, 'Termination Kits', '33Kv', 'Engineering', 'CHUANG XIONG GROUP TECHNOLOGY LTD', 'approved', NULL, NULL, '15-Jul-2020', '2020-07-15 11:26:39', 'PETER CHEGE KARIUKI'),
(21, 8, 'GI Sheet ', '8x4x1.00mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '14-Aug-2020', '2020-08-14 12:32:52', 'BENCO MBUGUA'),
(22, 9, 'MS Tee', ' 25x25x3', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:27:36', 'BENCO MBUGUA'),
(23, 10, 'flat Bar ', '  3/4\'\'x3mm', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:28:46', 'BENCO MBUGUA'),
(24, 11, 'GI Sheet', '8x4x1mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:30:37', 'BENCO MBUGUA'),
(25, 12, 'MIld steel Dia', '3/8\'\'', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:35:50', 'BENCO MBUGUA'),
(26, 13, 'Stainless steel Rod', 'Dia 8mm G304', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:39:10', 'BENCO MBUGUA'),
(27, 14, 'Werods', 'N3 E6013 3.2mm', 'Engineering', 'P&L Above Heights Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:42:52', 'BENCO MBUGUA'),
(28, 15, 'Zed Angle', '25x25x3mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:44:20', 'BENCO MBUGUA'),
(29, 16, 'SHS', '40x40x1.2mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:45:26', 'BENCO MBUGUA'),
(30, 17, 'SHS', '50x50x1.5mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:46:24', 'BENCO MBUGUA'),
(31, 18, 'Hinges', 'Window Hinges', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:47:29', 'BENCO MBUGUA'),
(32, 19, 'Flat Bar', '25x3mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '25-Aug-2020', '2020-08-25 09:48:53', 'BENCO MBUGUA'),
(33, 20, 'SHS', '50x50x1.5mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:49:12', 'BENCO MBUGUA'),
(34, 21, 'SHS', '40x40x1.5mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:50:07', 'BENCO MBUGUA'),
(35, 22, 'SHS', '20x20x1.2mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:50:47', 'BENCO MBUGUA'),
(36, 23, 'MS Plate', '8x4x1.5mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:51:32', 'BENCO MBUGUA'),
(37, 24, 'GAS', 'Oxygen', 'Engineering', 'Noble Gases', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:54:23', 'BENCO MBUGUA'),
(38, 25, 'Powder Coating Paint', 'PP STR KEN RAL', 'Engineering', 'Kansai Paint', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 08:59:04', 'BENCO MBUGUA'),
(39, 26, 'Flat Bar', '1\'\'x3mm', 'Engineering', 'Maisha Steel  Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 11:04:37', 'BENCO MBUGUA'),
(40, 27, 'Expanded Mesh', 'Expanded Mesh', 'Engineering', 'Clerb Enterprises Ltd', 'pending_approval', NULL, NULL, '28-Aug-2020', '2020-08-28 11:08:58', 'BENCO MBUGUA'),
(41, 28, 'MS Plate', '8x4x1.2mm', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:22:09', 'BENCO MBUGUA'),
(42, 29, 'Flat Bar', '1x3mm', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:23:46', 'BENCO MBUGUA'),
(43, 30, 'Flat Bar', '3/4x3mm', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:24:33', 'BENCO MBUGUA'),
(44, 31, 'SHS', '150x150x4mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:26:05', 'BENCO MBUGUA'),
(45, 32, 'RHS', '100X50X3MM', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:34:00', 'BENCO MBUGUA'),
(46, 33, 'Round Bar', '16mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:35:00', 'BENCO MBUGUA'),
(47, 34, 'Z Purlin', '5x2x2mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:35:51', 'BENCO MBUGUA'),
(48, 35, 'Tee', '1\'\'', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 06:37:20', 'BENCO MBUGUA'),
(49, 36, 'Angleline', '50x50x3mm', 'Engineering', 'Maisha Steel  Ltd', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:03:29', 'BENCO MBUGUA'),
(50, 37, 'Bolt & Nuts', '8mmx1\'\'', 'Engineering', 'Orbital Fastner', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:08:13', 'BENCO MBUGUA'),
(51, 38, 'Screws', 'Roofing 1\'\'', 'Engineering', 'P carlos hardware', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:09:20', 'BENCO MBUGUA'),
(52, 39, 'Rawl Bolt', '12mm Dia x100', 'Engineering', 'P carlos hardware', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:10:20', 'BENCO MBUGUA'),
(53, 40, 'Down Stoppers', 'Stopper', 'Engineering', 'P carlos hardware', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:11:07', 'BENCO MBUGUA'),
(54, 41, 'Hidges', 'Window Hidges', 'Engineering', 'P carlos hardware', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:11:45', 'BENCO MBUGUA'),
(55, 42, 'Paint', 'NC Primer Grey', 'Engineering', 'GYM  Car Auto Paints', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:23:13', 'BENCO MBUGUA'),
(56, 43, 'Thinner', 'STD Thinner', 'Engineering', 'GYM  Car Auto Paints', 'pending_approval', NULL, NULL, '17-Sep-2020', '2020-09-17 07:23:53', 'BENCO MBUGUA'),
(57, 44, 'Mild steel', '8x4x1mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:37:59', 'BENCO MBUGUA'),
(58, 45, 'Gas', 'Argon High Purity Gas', 'Engineering', 'Noble Gases', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:38:55', 'BENCO MBUGUA'),
(59, 46, 'SHS', '40x40x1.2', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:40:06', 'BENCO MBUGUA'),
(60, 47, 'Powder coating Paint', 'Red textures', 'Engineering', 'Akzonobel', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:44:11', 'BENCO MBUGUA'),
(61, 48, 'SS Rod', 'POM C Rod 25mm Dia', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:46:11', 'BENCO MBUGUA'),
(62, 49, 'Mild Steel', 'Mild steel 5/8mm dia', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:47:41', 'BENCO MBUGUA'),
(63, 50, 'SHS', '2x2x16G', 'Engineering', 'Monross Hardware', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:52:05', 'BENCO MBUGUA'),
(64, 51, 'Agnleline ', '2\'\'X3/16', 'Engineering', 'Monross Hardware', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:52:54', 'BENCO MBUGUA'),
(65, 52, 'Flat bar', '11/2x1/8', 'Engineering', 'Monross Hardware', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:53:33', 'BENCO MBUGUA'),
(66, 53, 'Zed bar', '1\'\'', 'Engineering', 'Monross Hardware', 'pending_approval', NULL, NULL, '07-Oct-2020', '2020-10-07 07:54:15', 'BENCO MBUGUA'),
(67, 54, 'SS plate', '8x4x2mm Grade 304', 'Engineering', 'APEX STEEL LTD', 'pending_approval', NULL, NULL, '21-Oct-2020', '2020-10-21 11:54:30', 'BENCO MBUGUA'),
(68, 55, 'SS Plate', '8x4x4mm Grade 304', 'Engineering', 'APEX STEEL LTD', 'pending_approval', NULL, NULL, '21-Oct-2020', '2020-10-21 11:55:34', 'BENCO MBUGUA'),
(69, 56, 'Mild Steel Plate', '8x4x3.8mm', 'Engineering', 'APEX STEEL LTD', 'pending_approval', NULL, NULL, '21-Oct-2020', '2020-10-21 11:56:20', 'BENCO MBUGUA'),
(70, 57, 'Mild steel Plate', '8x4x5.8mm', 'Engineering', 'APEX STEEL LTD', 'pending_approval', NULL, NULL, '21-Oct-2020', '2020-10-21 11:57:13', 'BENCO MBUGUA'),
(71, 58, 'Mild Steel Plate', '8x4x1.2mm', 'Engineering', 'APEX STEEL LTD', 'pending_approval', NULL, NULL, '21-Oct-2020', '2020-10-21 11:57:51', 'BENCO MBUGUA'),
(72, 59, 'MS Plate', '8x4x1mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '27-Oct-2020', '2020-10-27 09:28:51', 'BENCO MBUGUA'),
(73, 60, 'MS Plate', '8x4x3mm', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '27-Oct-2020', '2020-10-27 09:29:46', 'BENCO MBUGUA'),
(74, 61, 'Shaft', 'EN9 35mmx300mm', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '28-Oct-2020', '2020-10-28 08:39:02', 'BENCO MBUGUA'),
(75, 62, 'SS Sheet', '8x4x3mm G304', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '28-Oct-2020', '2020-10-28 08:40:21', 'BENCO MBUGUA'),
(76, 63, 'SS Round tube', '1/2x1.5x5.8mm', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '28-Oct-2020', '2020-10-28 08:41:39', 'BENCO MBUGUA'),
(77, 64, 'SS Bend', '11/2 G304', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '28-Oct-2020', '2020-10-28 08:42:38', 'BENCO MBUGUA'),
(78, 65, 'Powder Coating Paint', 'Glossy RAL013', 'Powder Coating', 'Nasib Industries Ltd', 'pending_approval', NULL, NULL, '28-Oct-2020', '2020-10-28 08:47:14', 'BENCO MBUGUA'),
(79, 66, 'Filler Wire', '2.4', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:43:12', 'BENCO MBUGUA'),
(80, 67, 'Filler wire', '1.60', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:43:56', 'BENCO MBUGUA'),
(81, 67, 'Filler wire', '1.60', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:44:08', 'BENCO MBUGUA'),
(82, 67, 'Filler wire', '1.60', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA'),
(83, 67, 'Filler wire', '1.60', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:44:12', 'BENCO MBUGUA'),
(84, 68, 'Slit Cutter', '7\'\'', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:45:23', 'BENCO MBUGUA'),
(85, 69, 'Pure Copper Rod', '25mmx1ft', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '29-Oct-2020', '2020-10-29 06:47:08', 'BENCO MBUGUA'),
(86, 70, 'SS Pipe G304', '50x1.5mm', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:05:17', 'BENCO MBUGUA'),
(87, 71, 'MS Plate', '8x4x1', 'Engineering', 'Central Auto & Hardware Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:06:56', 'BENCO MBUGUA'),
(88, 72, 'Gas', 'Argon High Purity', 'Engineering', 'Noble Gases', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:08:48', 'BENCO MBUGUA'),
(89, 73, 'SS Bend', 'G304 2\'\'', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:11:01', 'BENCO MBUGUA'),
(90, 74, 'SS Round Tube', 'Dia 1.5x1.5mmx5.8m G304', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:12:08', 'BENCO MBUGUA'),
(91, 75, 'SS Rosette cover', 'Cover 1.5 CC04', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:13:13', 'BENCO MBUGUA'),
(92, 76, 'SS Glass clamp', 'GC01', 'Engineering', 'Kens Metal Industries Ltd', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:14:01', 'BENCO MBUGUA'),
(93, 77, 'Wax Bar', 'Wax Bar', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:16:40', 'BENCO MBUGUA'),
(94, 78, 'Flap Disc', '120 & 80', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:17:29', 'BENCO MBUGUA'),
(95, 79, 'Slit cutter', 'Makita  4\'\'', 'Engineering', 'Fujjo Hardware', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:18:24', 'BENCO MBUGUA'),
(96, 80, 'Flap wheel', '120 &80', 'Engineering', 'Jumbo Hardware', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:24:35', 'BENCO MBUGUA'),
(97, 81, 'Sisal Cloth Disc', 'HD 7\'\'', 'Engineering', 'Jumbo Hardware', 'pending_approval', NULL, NULL, '04-Nov-2020', '2020-11-04 09:43:53', 'BENCO MBUGUA'),
(98, 82, 'Test Stock', 'Stock description', 'Engineering', 'Nail N Steels Ltd', 'pending_approval', NULL, NULL, '14-Feb-2222', '2022-02-14 12:43:38', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Active',
  `sector` varchar(100) DEFAULT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recorded_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_name`, `contact`, `email`, `status`, `sector`, `time_recorded`, `recorded_by`) VALUES
(34, 'ZHEJIANG YOMIN ELECTRIC LTD', '0086-577-61616258', 'info@zhejiangyomin.com', 'Active', '9', '2020-07-15 10:45:26', 'PETER CHEGE KARIUKI'),
(35, 'CHUANG XIONG GROUP TECHNOLOGY LTD', '0086-75523058522', 'info@chuangxiongroup.com', 'Active', '9', '2020-07-15 10:48:31', 'PETER CHEGE KARIUKI'),
(36, 'Central Auto & Hardware Ltd', '0725551960', 'centralautohardware@gmail.com', 'Active', '8', '2020-08-14 12:31:18', 'BENCO MBUGUA'),
(37, 'Nail N Steels Ltd', '0780226226', 'sales@nspltd.com', 'Active', '6', '2020-08-25 09:23:49', 'BENCO MBUGUA'),
(38, 'Kens Metal Industries Ltd', '733914172/2/3', 'sales.inda@kensmetal.co.ke', 'Active', '8', '2020-08-25 09:32:51', 'BENCO MBUGUA'),
(39, 'P&L Above Heights Ltd', '0783448888', 'plaboveheights786@gmail.com', 'Active', '6', '2020-08-25 09:41:00', 'BENCO MBUGUA'),
(40, 'Noble Gases', '0726870104', 'info@noblegases.co.ke', 'Active', '6', '2020-08-28 08:52:52', 'BENCO MBUGUA'),
(41, 'Kansai Paint', '0722 480 576', 'info@kansaipiascan.co.ke', 'Active', '8', '2020-08-28 08:57:27', 'BENCO MBUGUA'),
(42, 'Maisha Steel  Ltd', '0724165900', 'info@maishasteel.com', 'Active', '6', '2020-08-28 11:03:49', 'BENCO MBUGUA'),
(43, 'Clerb Enterprises Ltd', '0721628235', 'clerb@gmail.com', 'Active', '6', '2020-08-28 11:06:47', 'BENCO MBUGUA'),
(44, 'Orbital Fastner', '0721577548', 'orbitalfastnerltd@gmail.com', 'Active', '8', '2020-09-17 07:06:19', 'BENCO MBUGUA'),
(45, 'P carlos hardware', '0723649478', 'carlos@gmail.com', 'Active', '8', '2020-09-17 07:07:12', 'BENCO MBUGUA'),
(46, 'GYM  Car Auto Paints', '0757611499', 'gym@gmail.com', 'Active', '8', '2020-09-17 07:22:28', 'BENCO MBUGUA'),
(47, 'Akzonobel', '720204181', 'jacklenson.maina@akzonobel.com', 'Active', '8', '2020-10-07 07:43:19', 'BENCO MBUGUA'),
(48, 'Monross Hardware', '0720814723', 'Monross@gmail.com', 'Active', '8', '2020-10-07 07:51:10', 'BENCO MBUGUA'),
(49, 'APEX STEEL LTD', 'Brian', 'b.kitathe@apex-steel.com', 'Active', '8', '2020-10-21 11:49:00', 'BENCO MBUGUA'),
(50, 'Nasib Industries Ltd', 'Name', 'sales@glorypaints.or.ke', 'Active', '6', '2020-10-28 08:44:58', 'BENCO MBUGUA'),
(51, 'Fujjo Hardware', 'dan', 'Fujjo@gmail.com', 'Active', '6', '2020-10-29 06:42:14', 'BENCO MBUGUA'),
(52, 'Jumbo Hardware', 'Jumbo', 'Jumbo@gmail.com', 'Active', '6', '2020-11-04 09:19:31', 'BENCO MBUGUA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`email`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `all_evidence_document`
--
ALTER TABLE `all_evidence_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_end_delivery`
--
ALTER TABLE `customer_end_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_end_returns`
--
ALTER TABLE `customer_end_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_sector`
--
ALTER TABLE `customer_sector`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_approvers`
--
ALTER TABLE `delivery_approvers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`),
  ADD KEY `directorate_id` (`directorate_id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- Indexes for table `endproductresources`
--
ALTER TABLE `endproductresources`
  ADD PRIMARY KEY (`resource_id`);

--
-- Indexes for table `end_product`
--
ALTER TABLE `end_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_comments`
--
ALTER TABLE `general_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_issued`
--
ALTER TABLE `invoice_issued`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_issued_payment`
--
ALTER TABLE `invoice_issued_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_received`
--
ALTER TABLE `invoice_received`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_received_payment`
--
ALTER TABLE `invoice_received_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpesa_payments`
--
ALTER TABLE `mpesa_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_requests`
--
ALTER TABLE `page_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requested_by` (`requested_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_updates`
--
ALTER TABLE `product_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sign_in_logs`
--
ALTER TABLE `sign_in_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `single_product`
--
ALTER TABLE `single_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_users`
--
ALTER TABLE `staff_users`
  ADD PRIMARY KEY (`EmpNo`),
  ADD KEY `DepartmentCode` (`DepartmentCode`);

--
-- Indexes for table `stocks_returns`
--
ALTER TABLE `stocks_returns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_approvers`
--
ALTER TABLE `stock_approvers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_item`
--
ALTER TABLE `stock_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;

--
-- AUTO_INCREMENT for table `all_evidence_document`
--
ALTER TABLE `all_evidence_document`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customer_end_delivery`
--
ALTER TABLE `customer_end_delivery`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_end_returns`
--
ALTER TABLE `customer_end_returns`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_sector`
--
ALTER TABLE `customer_sector`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `delivery_approvers`
--
ALTER TABLE `delivery_approvers`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `end_product`
--
ALTER TABLE `end_product`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `general_comments`
--
ALTER TABLE `general_comments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `invoice_issued`
--
ALTER TABLE `invoice_issued`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_issued_payment`
--
ALTER TABLE `invoice_issued_payment`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_received`
--
ALTER TABLE `invoice_received`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `invoice_received_payment`
--
ALTER TABLE `invoice_received_payment`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mpesa_payments`
--
ALTER TABLE `mpesa_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_requests`
--
ALTER TABLE `page_requests`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1767;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `payment_gateway`
--
ALTER TABLE `payment_gateway`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_updates`
--
ALTER TABLE `product_updates`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sign_in_logs`
--
ALTER TABLE `sign_in_logs`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `single_product`
--
ALTER TABLE `single_product`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `stocks_returns`
--
ALTER TABLE `stocks_returns`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_approvers`
--
ALTER TABLE `stock_approvers`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `stock_item`
--
ALTER TABLE `stock_item`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
